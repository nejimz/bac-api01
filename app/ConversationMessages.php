<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use \App\Users;

class ConversationMessages extends Model
{
    protected $table = "conversation_messages";
    public $timestamps = false;

    public function message_conversations()
    {
    	return $this->belongsTo('App\Conversations', 'conversation_id');
    }

    public function message_user()
    {
    	//return Users::whereNtlogin($this->ntlogin)->first();
    	return $this->belongsTo('App\Users', 'ntlogin', 'ntlogin');
    }

    // public function message_users()
    // {
    // 	return $this->belongsTo('App\Users', 'user_id');
    // }
}
