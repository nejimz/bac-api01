<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ITSystemsWorkstationDeviceNumber extends Model
{
    protected $connection 	= 'bac_scanner';
    protected $table 		= 'Core_Assessment.UniqueDevices';
    public $timestamps 		= false;

    public function getComputerNameFormatAttribute()
    {
    	return str_replace('.panasiaticsolutions.net', '', $this->ComputerName);
    }
}
