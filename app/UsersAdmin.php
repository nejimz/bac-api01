<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersAdmin extends Model
{
    //
     public $table = 'users_admin';
     public $timestamps = false;
}
