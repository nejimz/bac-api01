<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ITSystemsWorkWinAssessmentWindowsInstalledSoftwareFull extends Model
{
    protected $connection  = 'bac_scanner';
    protected $table 		= 'Win_Assessment.WindowsInstalledSoftwareFull';
    public $timestamps 		= false;

    public function getInstallDateFormatAttribute()
    {
    	return \Carbon\Carbon::parse( $this->InstallDate )->format('Y-m-d');
    }

    public function getSoftwareCountAttribute()
    {
        return ITSystemsWorkWinAssessmentWindowsInstalledSoftwareFull::where('Name', $this->Name)->count('Name');
    }
}
