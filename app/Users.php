<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Users extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    public $timestamps = false;
    protected $table = 'users';
    protected $fillable = ['name', 'password', 'api_token'];
    protected $hidden = ['password', 'remember_token', 'api_token'];

    public function scopeUserInformation($query, $ntlogin)
    {
        return $query->where('ntlogin', '=', $ntlogin)
            ->select(['employee_number', 'name', 'ntlogin', 'ou', 'image'])
            ->first();
    }

    public function scopeUserMemberof($ntlogin)
    {

    }
}
