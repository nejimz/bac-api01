<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ITSystemsWorkstationHardwareInventoryEx extends Model
{
    protected $connection  = 'bac_scanner';
    protected $table 		= 'AllDevices_Assessment.HardwareInventoryEx';
    public $timestamps 		= false;

    public function getComputerModelsCountAttribute()
    {
    	return ITSystemsWorkstationHardwareInventoryEx::where('ComputerModel', $this->ComputerModel)->count('ComputerModel');
    }

    public function getCpuCountAttribute()
    {
    	return ITSystemsWorkstationHardwareInventoryEx::where('Cpu', $this->Cpu)->count('Cpu');
    }

    public function getNetworkAdapterCountAttribute()
    {
    	return ITSystemsWorkstationHardwareInventoryEx::where('ActiveNetworkAdapter', $this->ActiveNetworkAdapter)->count('ActiveNetworkAdapter');
    }

    public function getSystemMemoryCountAttribute()
    {
    	return ITSystemsWorkstationHardwareInventoryEx::where('SystemMemory', $this->SystemMemory)->count('SystemMemory');
    }

    public function getVideoCardCountAttribute()
    {
    	return ITSystemsWorkstationHardwareInventoryEx::where('VideoCard', $this->VideoCard)->count('VideoCard');
    }

    public function getSoundCardCountAttribute()
    {
    	return ITSystemsWorkstationHardwareInventoryEx::where('SoundCard', $this->SoundCard)->count('SoundCard');
    }

    public function getBiosCountAttribute()
    {
    	return ITSystemsWorkstationHardwareInventoryEx::where('BIOs', $this->BIOs)->count('BIOs');
    }

    public function getDiskDriveCountAttribute()
    {
        return ITSystemsWorkstationHardwareInventoryEx::where('DiskDrive', $this->DiskDrive)->count('DiskDrive');
    }

    public function getWMIStatusCountAttribute()
    {
        return ITSystemsWorkstationHardwareInventoryEx::where('WMIStatus', $this->WMIStatus)->count('WMIStatus');
    }
}
