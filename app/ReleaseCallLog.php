<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class ReleaseCallLog extends Model
{
    // 
    public $table = 'release_call_log';

    public function setUpdatedAtAttribute($value)
	{
	// Do nothing.
	}

	public function reason()
	{
		return $this->belongsTo('App\ReleaseCallValidReasons','reason_ID','reason_ID');
	}

	public function scopeTimeReport($query, $start, $end)
	{
		//
		$query->whereBetween('created_at', array($start, $end));

		return $query;
	}

	public function scopeTeamMembers($query, $employee_number)
	{
		$employee_number = $employee_number;

		$query->whereIn('employee_ID', function($query) use ($employee_number){
				$query->select('employee_ID')
						->from('call_center_roster')
						->where('supervisor_ID', $employee_number);
		})->orderBy('employee_ID', 'reason_ID');

		return $query;
	}

	public function scopeCountReasons($query, $employee_number)
	{
		$employee_number = $employee_number;

		$query->select(DB::raw('employee_ID, reason_ID, count(reason_ID) as reason_count, created_at'))
		->whereIn('employee_ID', function($query) use ($employee_number){

			if(is_null($employee_number))
			{
				$query->select('employee_ID')->from('call_center_roster')->distinct();
			}
			else
			{
				$query->select('employee_ID')->from('call_center_roster')->where('supervisor_ID', $employee_number);
			}
		})
		->groupBy('employee_ID', 'reason_ID', 'created_at')
		->orderBy('employee_ID');

		return $query;
	}

	public function getRosterAttribute()
	{
    	return Roster::RetrieveProfile($this->employee_ID)->orderBy('week_ending', 'DESC')->first();
	}

}
