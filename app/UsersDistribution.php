<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersDistribution extends Model
{
    //
    public $table = 'users_distribution';
    public $timestamps = false;

    protected $fillable = [ 'ntlogin', 'distribution_id' ];

    public function users()
    {
    	return $this->hasMany('App\Distributions', 'id', 'distribution_id');
    }
}