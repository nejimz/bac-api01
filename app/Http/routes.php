<?php
/*$app->get('/', function () use ($app) {
	return route('its_ws_summary_wmi_status');
    return $app->version();
});

\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
    \Log::info($query->sql, $query->bindings, $query->time);
});
*/
$app->get('/', ['middleware' => 'AllowCors', 'uses' => 'DashboardController@index']);

$app->get('announcement', ['middleware' => 'AllowCors', 'uses' => 'DashboardController@announcements']);

$app->group(['prefix' => 'it-systems', 'middleware' => 'AllowCors', 'namespace' => 'App\Http\Controllers'], function() use ($app){
	$app->get('workstation/summary/wmi-status', ['uses' => 'ITSystemsWorkStationController@summary_wmi_status']);
	$app->get('workstation/summary/disk-drive', ['uses' => 'ITSystemsWorkStationController@summary_disk_drive']);
	$app->get('workstation/summary/sound-card', ['uses' => 'ITSystemsWorkStationController@summary_sound_card']);
	$app->get('workstation/summary/bios', ['uses' => 'ITSystemsWorkStationController@summary_bios']);
	$app->get('workstation/summary/video-card', ['uses' => 'ITSystemsWorkStationController@summary_video_card']);
	$app->get('workstation/summary/system-memory', ['uses' => 'ITSystemsWorkStationController@summary_system_memory']);
	$app->get('workstation/summary/network-adapter', ['uses' => 'ITSystemsWorkStationController@summary_network_adapter']);
	$app->get('workstation/summary/cpu', ['uses' => 'ITSystemsWorkStationController@summary_cpu']);
	$app->get('workstation/summary/operating-system', ['uses' => 'ITSystemsWorkStationController@summary_operating_system']);
	$app->get('workstation/summary/computer-model', ['uses' => 'ITSystemsWorkStationController@summary_computer_model']);
	$app->get('workstation/summary/software-installed', ['uses' => 'ITSystemsWorkStationController@summary_software_installed']);
});

$app->group(['prefix' => 'release-call-log', 'middleware' => 'AllowCors', 'namespace' => 'App\Http\Controllers'], function() use ($app){
	$app->get('download', ['uses' => 'ReleaseCallLogController@download']);
	$app->get('download-json', ['uses' => 'ReleaseCallLogController@downloadJSON']);
	$app->get('genreport', ['uses' => 'ReleaseCallLogController@generatereport']);
});

$app->group(['prefix' => 'agentdtr', 'middleware' => 'AllowCors', 'namespace' => 'App\Http\Controllers'], function() use ($app){
	$app->get('getdtr', ['uses' => 'AgentDtrController@getDtr']);
	$app->get('getdtr/breakdown', ['uses' => 'AgentDtrController@getDtrBreakdown']);
	$app->get('getagents', ['uses' => 'AgentDtrController@getAgentOnLilo']);
	$app->get('test', ['uses' => 'AgentDtrController@getAgentTest']);
});

$app->group(['prefix' => 'transfer-log', 'middleware' => 'AllowCors', 'namespace' => 'App\Http\Controllers'], function() use ($app){
	$app->get('download', ['uses' => 'TransferLogController@download']);
});

$app->group(['prefix' => 'messaging', 'middleware' => 'AllowCors', 'namespace' => 'App\Http\Controllers'], function() use ($app){
	$app->get('{ntlogin}/inbox/total/unread', ['uses' => 'ConversationsController@inbox_total_unread']);
	$app->get('{ntlogin}/inbox', ['uses' => 'ConversationsController@inbox']);
	$app->get('{ntlogin}/message', ['uses' => 'ConversationsController@message']);
	$app->get('{ntlogin}/message/delete', ['uses' => 'ConversationsController@delete']);
});