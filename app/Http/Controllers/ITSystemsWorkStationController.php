<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ITSystemsWorkstationDeviceNumber;
use App\ITSystemsWorkstationHardwareInventoryCore;
use App\ITSystemsWorkstationHardwareInventoryEx;
use App\ITSystemsWorkStationInstalledProducts;
use App\ITSystemsWorkWinAssessmentWindowsInstalledSoftwareFull;
use Illuminate\Http\Request;

class ITSystemsWorkStationController extends Controller
{
    /**

    **/
    public function summary_wmi_status(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'WMIStatus', DB::raw("COUNT(WMIStatus) AS data_count") )
       # $data = DB::table('AllDevices_Assessment.HardwareInventoryEx')
                #->select( 'WMIStatus', DB::raw("COUNT(WMIStatus) AS data_count") )
                ->whereNotNull('WMIStatus')
                ->groupBy('WMIStatus')
                ->orderBy('data_count', 'DESC')
                ->orderBy('WMIStatus', 'ASC')
                ->get();
                
        #return $data;
        return response()->json($data)->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_disk_drive(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'DiskDrive', DB::raw("COUNT(DiskDrive) AS data_count") )
                ->whereNotNull('DiskDrive')
                ->groupBy('DiskDrive')
                ->orderBy('data_count', 'DESC')
                ->orderBy('DiskDrive', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_sound_card(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'SoundCard', DB::raw("COUNT(SoundCard) AS data_count") )
                ->whereNotNull('SoundCard')
                ->groupBy('SoundCard')
                ->orderBy('data_count', 'DESC')
                ->orderBy('SoundCard', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_bios(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'BIOs', DB::raw("COUNT(BIOs) AS data_count") )
                ->whereNotNull('BIOs')
                ->groupBy('BIOs')
                ->orderBy('data_count', 'DESC')
                ->orderBy('BIOs', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_video_card(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'VideoCard', DB::raw("COUNT(VideoCard) AS data_count") )
                ->whereNotNull('VideoCard')
                ->groupBy('VideoCard')
                ->orderBy('data_count', 'DESC')
                ->orderBy('VideoCard', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_system_memory(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'SystemMemory', DB::raw("COUNT(SystemMemory) AS data_count") )
                ->whereNotNull('SystemMemory')
                ->groupBy('SystemMemory')
                ->orderBy('data_count', 'DESC')
                ->orderBy('SystemMemory', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_network_adapter(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'ActiveNetworkAdapter', DB::raw("COUNT(ActiveNetworkAdapter) AS data_count") )
                ->whereNotNull('ActiveNetworkAdapter')
                ->groupBy('ActiveNetworkAdapter')
                ->orderBy('data_count', 'DESC')
                ->orderBy('ActiveNetworkAdapter', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_cpu(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'Cpu', DB::raw("COUNT(Cpu) AS data_count") )
                ->whereNotNull('Cpu')
                ->groupBy('Cpu')
                ->orderBy('data_count', 'DESC')
                ->orderBy('Cpu', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_operating_system(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryCore::select( 'CurrentOperatingSystem', DB::raw("COUNT(CurrentOperatingSystem) AS data_count") )
                ->whereNotNull('CurrentOperatingSystem')
                ->groupBy('CurrentOperatingSystem')
                ->orderBy('data_count', 'DESC')
                ->orderBy('CurrentOperatingSystem', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }

    /**

    **/
    public function summary_computer_model(Request $request)
    {
        $data = ITSystemsWorkstationHardwareInventoryEx::select( 'ComputerModel', DB::raw("COUNT(ComputerModel) AS data_count") )
                ->whereNotNull('ComputerModel')
                ->groupBy('ComputerModel')
                ->orderBy('data_count', 'DESC')
                ->orderBy('ComputerModel', 'ASC')
                ->get();

        return response()->json( $data->toArray() )->setCallback( $request->callback );
    }

    public function summary_software_installed(Request $request)
    {
        $data = ITSystemsWorkWinAssessmentWindowsInstalledSoftwareFull::select( 'Name', DB::raw("COUNT(Name) AS data_count") )
                ->whereNotNull('Name')
                ->groupBy('Name')
                ->orderBy('data_count', 'DESC')
                ->orderBy('Name', 'ASC')
                ->get();
                
        #return $data->toJson();
        return response()->json($data->toArray())->setCallback( $request->callback );
    }
}
