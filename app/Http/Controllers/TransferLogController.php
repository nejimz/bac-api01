<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use App\Roster;
use App\Accounts;
use App\Skills;
use App\TransferLogTransactions;
use App\TransferLogCallDetails;
use App\TransferLog;

class TransferLogController extends Controller
{
    public function callsreceiveddropdown(Request $request)
    {
       //LOAD DROPDOWN FOR SELECTED TRANSACTION
        $options = '<option value="0">-- SELECT --</option>';
        $calls = TransferLogCallDetails::where('transaction_id', '=', $request['transID'])->get();
        foreach ($calls as $callDets) {
            $options .= '<option value="'.$callDets['id'].'">'.$callDets['call_details'].'</option>';
        }

        return $options;
    }

    public function extdept(Request $request)
    {
       //LOAD EXTENSTION NUMBER AND DEPARTMENT FOR SELECTED CALL RCVD
        $calls = TransferLogCallDetails::where('id', '=', $request['callId'])->first();
        $extensionDepartment = $calls->department." - ".$calls->extension;

        return $extensionDepartment;
    }

    public function download(Request $request)
    {
        if($request->reports_start >= $request->reports_end)
        {
            echo "<center><h3>Invalid date argument.</h3></center>";
        }
        else
        {
            $startDate = Carbon::parse($request->reports_start);
            $endDate = Carbon::parse($request->reports_end);

            //dump($startDate);

            //dump($endDate);

            //$startDate->timezone('Asia/Manila');
            //$endDate->timezone('Asia/Manila');

            //dump($startDate);

            //dump($endDate);

            $table_result =  TransferLog::with( [ 'AccountName', 'SkillName', 'TransactionName', 'CallDetailsName' ] )->whereBetween('created_at', [ $startDate, $endDate ] )->get();

            //dump($table_result); exit;
            
            $report_table = '<table border="1"><tr><th>Emp. No.</th><th>AVAYA</th><th>Account</th><th>Skill</th><th>Transaction</th><th>Call Details</th><th>Department</th><th>Extension</th><th>Notes</th><th>Logged</th></tr>';

            foreach ($table_result as $report) 
            {
                $agentData = Roster::RetrieveProfile( $report->employee_ID )->orderBy('week_ending', 'DESC')->first();
                $reportDate = Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at, 'Asia/Manila')->timezone('America/New_York');
                #echo '<pre>'; dd($report->AccountName->account); echo '</pre>';
                $report_table .= '</tr><td>' . $report->employee_ID . 
                                 '</td><td>' . $agentData->avaya . 
                                 '</td><td>' . $report->AccountName->account . 
                                 '</td><td>' . $report->SkillName->skill . 
                                 '</td><td>' . $report->TransactionName->transaction . 
                                 '</td><td>' . $report->CallDetailsName->call_details . 
                                 '</td><td>' . $report->CallDetailsName->department . 
                                 '</td><td>' . $report->CallDetailsName->extension . 
                                 '</td><td>' . $report->transfer_log_notes . 
                                 '</td><td>' . $reportDate->toDateTimeString() . 
                                 '</td></tr>';
            }
            
            $report_table .= '</table>';

            return response($report_table, 200)->header('Content-Type', 'application/vnd.ms-excel');
        }      
    }
}
