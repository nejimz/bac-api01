<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenusController extends Controller
{
    public function menus($ou)
    {
    	if ($ou == 'IT Systems')
    	{
    		if(1 != 0)
    		{
    			return 'Programmer';
    		}
    		else
    		{
    			return 'IT';
    		}
    	}
    	elseif(in_array($ou, ['ERD','Legacy','PagePlus','SafeLink','SimpleMobile','StraightTalk','SupGroup']))
    	{
    		return 'Agent';
    	}
    	elseif($ou == 'Quality')
    	{
    		return 'Quality';
    	}
    	elseif($ou == 'Supervisors')
    	{
    		return 'Supervisors';
    	}
    	elseif($ou == 'CA')
    	{
    		return 'CA';
    	}
    	elseif($ou == 'HRD')
    	{
    		return 'HRD';
    	}
    	elseif($ou == 'Managers')
    	{
    		return 'Managers';
    	}
    	elseif($ou == 'RAPID')
    	{
    		return 'RAPID';
    	}
    	elseif($ou == 'Accounting')
    	{
    		return 'Accounting';
    	}
    	elseif($ou == 'Recruitment')
    	{
    		return 'Recruitment';
    	}
    	elseif(in_array($ou,['Training','SME']))
    	{
    		return 'Training and SME';
    	}
    	elseif($ou == 'Trainee')
    	{
    		return 'Trainee';
    	}
    	elseif($ou == 'CMSGroup')
    	{
    		return 'CMSGroup';
    	}
    	elseif($ou == 'Coordinators')
    	{
    		return 'Coordinators';
    	}
    	elseif($ou == 'Receptionist')
    	{
    		return 'Receptionist';
    	}
    }
}
