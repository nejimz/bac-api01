<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Announcements;
use App\UsersAdmin;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
    Index
    **/
    public function index()
    {
        #return redirect('http://bac-dev04/');
        echo "Welcome to PUSH API server.";
    }

    /**
    Authentication Set Cookie
    **/
    public function authCookie()
    {
        echo "Welcome to PUSH API server.";
    }

    /**
    announcement
    **/
    public function announcements(Request $request)
    {
        $n = 0;
        $data = [];
        $announcements = Announcements::with('user')->where('active', 1)
                        #->leftJoin('users', 'users.ntlogin', '=', 'announcements.ntlogin')
                        ->orderBy('created_at', 'DESC')
                        ->paginate(5);

        foreach ($announcements as $row)
        {
            $data[$n] = [
                'created_at' => Carbon::parse($row->created_at)->toDayDateTimeString(),
                'message' => $row->message,
                'name' => $row->user->name,
            ];
            $n++;
        }
        //return response()->json( $data )->setCallback( $request->callback );
        return response()->json( $data );
    }
}
