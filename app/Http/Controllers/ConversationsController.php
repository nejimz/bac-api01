<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Users;
use App\Conversations;
use App\ConversationMembers;
use App\ConversationMessages;
use DB;
use Carbon\Carbon;

class ConversationsController extends Controller
{
    public function conversation_messages($ntlogin, $read)
    {
        return  ConversationMessages::with(['message_conversations', 'message_conversations.conversation_messages', 'message_conversations.conversation_messages.message_user', 
                        'message_conversations.conversation_members' => function($query) use ($ntlogin){
                            $query->where('ntlogin', $ntlogin);
                    }])
                ->whereHas('message_conversations.conversation_members', function($query) use ($ntlogin, $read){
                    if($read == 0)
                    {
                        $query->whereNtlogin( $ntlogin )->whereNull('last_view')->whereDeletedAt(0);
                    }
                    else
                    {
                        $query->whereNtlogin( $ntlogin )->whereDeletedAt(0);
                    }
                });
    }

    /*public function unread_messages($ntlogin)
    {
        return ConversationMembers::whereNtlogin($ntlogin)->whereNull('last_view')->count();
    }*/

    public function inbox_total_unread(Request $request, $ntlogin = null)
    {
        $data = [];
        $total_count = 0;
        $data['total_count'] = ConversationMembers::whereNtlogin($ntlogin)->whereNull('last_view')->count();

        return response()->json( $data );
    }

    public function inbox(Request $request, $ntlogin = null)
    {
        $n = 1;
        $data = [];
        $search = ($request->has('search'))? trim($request->search) : '';
        $read   = ($request->has('read'))? trim($request->read) : 1;
        $query = $this->conversation_messages($ntlogin, $read);

        if($search != '')
        {
            $query->where(function($query) use ($search){
                $query->whereHas('message_conversations', function($query) use ($search){
                    $query->where('subject', 'LIKE', "%$search%");
                })
                ->orWhere(function($query) use ($search){
                    $query->whereHas('message_conversations.conversation_messages.message_user', function($query) use ($search){
                        $query->where('name', 'LIKE', "%$search%");
                    });
                });
            });
        }

        $messages = $query->select(DB::raw('MAX(created_at) AS created_at'),'conversation_id')
                    ->groupBy('conversation_id')->orderBy(DB::raw('MAX(created_at)'), 'DESC')->paginate(15);
        
        $data['total'] = $messages->total();
        $data['next_page_url'] = $messages->nextPageUrl();
        
        foreach ( $messages as $message )
        {
            $read_status = 1;
            $subject = $message->message_conversations->subject;
            $sender = $message->message_conversations->conversation_messages->first()->message_user->name;
            $created_at = $message->created_at;
            $created_at_format = Carbon::parse( $created_at )->diffForHumans();
            $members = $message->message_conversations->conversation_members->first();
            #$read_status = (is_null($members->last_view))? 0 : 1;

            if( is_null($members->last_view) || $members->last_view <  $created_at )
            {
                $read_status = 0;
            }

            $data['messages'][ $n ] = [
                'conversation_id' => $message->conversation_id,
                'conversation_member_id' => $members->id,
                'subject' => $subject,
                'sender' => $sender,
                'last_view' => $members->last_view,
                'created_at_format' => $created_at_format,
                'created_at' => $created_at,
                'read' => $read_status
            ];

            $n++;
        }

        return response()->json( $data );
    }

    public function message(Request $request)
    {

        if( !$request->has('conversation_id') || !$request->has('conversation_member_id') )
        {
            $data['error'] = true;
            $data['success'] = false;

            return response()->json( $data )->setCallback( $request->callback );
        }

        $n = 0;
        $x = 0;
        $data['error'] = false;
        $data['success'] = true;
        $data['messages'] = [];
        $data['recepients'] = [];

        $conversation_id = trim($request->conversation_id);
        $conversation_member_id = trim($request->conversation_member_id);
        // First Click
        $member = ConversationMembers::whereId($conversation_member_id)->first();
        // Update last view after click
        ConversationMembers::whereId($conversation_member_id)->update( [ 'last_view' => Carbon::now() ] );
        // My thread
        $thread = ConversationMessages::with('message_conversations', 'message_conversations.conversation_messages.message_user')
                    ->whereConversationId( $conversation_id )->orderBy('created_at', 'DESC')->get();

        foreach ($thread as $message)
        {
            $created_at = Carbon::parse($message->created_at);
            $read = true;

            if( is_null( $member->last_view ) )
            {
                $read = false;
            }

            $name = $message->message_conversations->conversation_messages->where('ntlogin', $message->ntlogin)->first()->message_user->name;
            $data['subject'] = $message->message_conversations->subject;
            $data['thread'][$n] = [
                'read' => $read,
                'created_at' => Carbon::parse($message->created_at)->toDayDateTimeString(),
                'name' => $name,
                'ntlogin' => $message->ntlogin,
                'message' => $message->message
            ];
            $n++;
        }
        
        $groups = [];
        $members = ConversationMembers::with('member_user')
                    ->whereConversationId($conversation_id)->orderBy('group', 'ASC')->orderBy('ntlogin', 'ASC')->get();

        foreach ($members as $row)
        {
            if($row->group != '')
            {
                if(!in_array($row->group, $groups) )
                {
                    $groups[$x] = $row->group;

                    $data['recepients'][$x] = [
                        'value' => $row->group,
                        'type' => 'group'
                    ];
                }
            }
            else
            {
                if($row->check_ntlogin_exists == 0)
                {
                    $data['recepients'][$x] = [
                        'value' => $row->ntlogin,
                        'type'  => 'ntlogin'
                    ];
                }
                else
                {
                    $data['recepients'][$x] = [
                        'value' => $row->member_user->name,
                        'type'  => 'ntlogin'
                    ];
                }
            }

            $x++;
        }
        
        return response()->json( $data );
    }

    public function delete(Request $request)
    {
        $data = [];
        $members_id = $request->messages;
        ConversationMembers::whereIn('id', $members_id)->update( [ 'deleted_at' => 1 ] );
        $data['success'] = true;
        return response()->json( $data );
    }
}
