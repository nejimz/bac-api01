<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Roster;
use App\ReleaseCallValidReasons;
use App\ReleaseCallLog;
use App\UsersReleaseCallLog;
use DateInterval;
use DatePeriod;


class ReleaseCallLogController extends Controller
{
    /**
    Download Generate Report
    **/
    public function generatereport(Request $request)
    {
        $data = [ 'records' => [], 'error' => 0 ];

       if($request->start >= $request->end)
        {
            $data['error'] = 1;
        }
        else
        {
            $supervisor = null;
            $table      = '';
            $startDate  = Carbon::createFromFormat('Y-m-d H:i:s', $request->start);
            $endDate    = Carbon::createFromFormat('Y-m-d H:i:s', $request->end);

            $query      = ReleaseCallLog::with('reason')->where(function($query) use ($startDate, $endDate){
                $query->whereBetween('created_at', [$startDate, $endDate]);
            });

            if($request->has('supervisor'))
            {
                $supervisor = $request->supervisor;
            }

            $logs = ReleaseCallLog::CountReasons( $supervisor )->TimeReport($startDate, $endDate)->get();

            $n = 0;

            foreach ($logs as $log) 
            {   
                $dateCarbon = strtotime(date($log->created_at));
                $agentData = Roster::RetrieveProfile($log->employee_ID)
                             ->whereAnd('week_ending', date('Y-m-d', strtotime('next sunday', $dateCarbon)))->first();
                
                $data['records'][$n]['employee_number'] = $log->employee_ID;
                $data['records'][$n]['avaya']           = $agentData->avaya;
                $data['records'][$n]['valid_reason']    = $log->reason->valid_reason;
                $data['records'][$n]['reasong_count']   = $log->reason_count;
                
                $n++;
            }

            return response()->json($data)->setCallback( $request->callback );
        }
    }

    /**
    Download RCL
    **/
    public function download(Request $request)
    {
        if($request->start >= $request->end)
        {
            echo "<div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Invalid date argument.</div>";
        }
        else
        {
            $content = "<table border=\"1\"><thead><tr>" . #
            "<th>Employee Number</th>" . #
            "<th>Avaya</th>" . #
            "<th>Reason</th>" . #
            "<th>Logged</th>" . #
            "</tr></thead>";

            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->start);
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->end);

            if($request->has('supervisor'))
            {
                $interval = new DateInterval('P1D');
                $daterange = new DatePeriod($startDate, $interval ,$endDate);

                $range = [];
                $rangedate = [];

                foreach ($daterange as $date) {
                    $range[\Carbon\Carbon::parse($date->format('Y-m-d H:i:s'))->endOfWeek()->toDateString()][] = $date->format('Y-m-d H:i:s');
                }

                foreach ($range as $key => $value) { 
                    $range_date[$key]['start'] = date("Y-m-d", strtotime($value[0])) ." ". date("H:i:s", strtotime($startDate)); 
                    $range_date[$key]['end'] = date("Y-m-d", strtotime($value[count($value)-1])) ." ". date("H:i:s", strtotime($endDate));                  
                }

                foreach ($range_date as $key => $value) {
                    $start = $value['start'];
                    $end = $value['end'];
                    $employee_number = $request->supervisor;

                    $query =  ReleaseCallLog::with('reason')->where(function($fquery) use ($start, $end){
                                                    $fquery->whereBetween('created_at', [$start, $end]);
                                            })->whereIn('employee_ID', function($squery) use ($employee_number, $key){
                                                    $squery->select('employee_ID')
                                                            ->from('call_center_roster')
                                                            ->where('week_ending', $key)
                                                            ->where('supervisor_ID', $employee_number);
                                            })->orderBy('created_at', 'ASC')->get();   

                    $content .= Self::getCSVContent($query); 
                }
            }
            else
            {
                $query =  ReleaseCallLog::with('reason')->where(function($query) use ($startDate, $endDate){
                    $query->whereBetween('created_at', [$startDate, $endDate]);
                });

                 $results = $query->orderBy('created_at', 'DESC')->get();
                 $content .= Self::getCSVContent($results, $content); 
            }         
        
            $content .= "</table>";
            
            //echo $content; exit;
            $headers    = [ 'content-type' => 'application/vnd.ms-excel' ];
            
            return response($content, 200)->header('Content-Type', 'application/vnd.ms-excel');
        }      
    }

    public function downloadJSON(Request $request)
    {
        if($request->start >= $request->end)
        {
            echo "<div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Invalid date argument.</div>";
        }
        else
        {
            $content = [];

            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->start);
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->end);

            if($request->has('supervisor'))
            {
                $interval = new DateInterval('P1D');
                $daterange = new DatePeriod($startDate, $interval ,$endDate);

                $range = [];
                $rangedate = [];

                foreach ($daterange as $date) {
                    $range[\Carbon\Carbon::parse($date->format('Y-m-d H:i:s'))->endOfWeek()->toDateString()][] = $date->format('Y-m-d H:i:s');
                }

                foreach ($range as $key => $value) { 
                    $range_date[$key]['start'] = date("Y-m-d", strtotime($value[0])) ." ". date("H:i:s", strtotime($startDate)); 
                    $range_date[$key]['end'] = date("Y-m-d", strtotime($value[count($value)-1])) ." ". date("H:i:s", strtotime($endDate));                  
                }

                foreach ($range_date as $key => $value) {
                    $start = $value['start'];
                    $end = $value['end'];
                    $employee_number = $request->supervisor;

                    $query =  ReleaseCallLog::with('reason')->where(function($fquery) use ($start, $end){
                                                    $fquery->whereBetween('created_at', [$start, $end]);
                                            })->whereIn('employee_ID', function($squery) use ($employee_number, $key){
                                                    $squery->select('employee_ID')
                                                            ->from('call_center_roster')
                                                            ->where('week_ending', $key)
                                                            ->where('supervisor_ID', $employee_number);
                                            })->orderBy('created_at', 'ASC')->get();   

                    foreach ($query as $row)
                    {
                        $reportDate = Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at, 'Asia/Manila')->timezone('America/New_York');
                        $content[] = [ 'EmployeeID' => $row->employee_ID, 
                                       'Avaya' => $row->roster->avaya,
                                       'Reason' => $row->reason->valid_reason,
                                       'Date' => $reportDate->toDateTimeString() ];
                    } 
                }
            }
            else
            {
                $query =  ReleaseCallLog::with('reason')->where(function($query) use ($startDate, $endDate){
                    $query->whereBetween('created_at', [$startDate, $endDate]);
                });

                $results = $query->orderBy('created_at', 'DESC')->get();

                foreach ($results as $row)
                {
                    $reportDate = Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at, 'Asia/Manila')->timezone('America/New_York');

                    $content[] = [ 'EmployeeID' => $row->employee_ID, 
                                    'Avaya' => $row->roster->avaya,
                                    'Reason' => $row->reason->valid_reason,
                                    'Date' => $reportDate->toDateTimeString() ];
                } 
            }         

            $data = compact('content');
            
            return response()->json($data)->setCallback( $request->callback );
        }      
    }

    public function getCSVContent($results)
    {            
        //dump($results);
        $content = '';
        foreach ($results as $row)
        {
           // dump($row);
            $reportDate = Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at, 'Asia/Manila')->timezone('America/New_York');

            $content .= "<tr>" . 
                      "<td>" . $row->employee_ID .
                      "</td><td>" . $row->roster->avaya .
                      "</td><td>" . $row->reason->valid_reason .
                      "</td><td>" . $reportDate .
                      "</td>" . 
                      "</tr>";
        }

        return $content;
    }
}
