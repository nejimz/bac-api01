<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Users;
use App\AgentDtrLilo;
use Carbon\Carbon;
use PDO;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;

class AgentDtrController extends Controller
{
	public function getDtr(Request $request)
    {
    	//SET DATABASE CONNECTION START
		$db = DB::connection('mysql3')->getPdo();
		//SET DATABASE CONNECTION END

		$empid = $request->empid;
		$ppay = $request->ppay;

		$yperiod = date("Y", strtotime($ppay));
		$pperiod = date("d", strtotime($ppay));
		$mperiod = date("m", strtotime($ppay));

		$data = [];

		if($pperiod == '15')
		{
			// get 
			$start_cutoff = date("Y-m-21", strtotime("-1 month", strtotime($ppay)));
			$data['CutOff']['Start'] = $start_cutoff;
			$end_cutoff = date("$yperiod-$mperiod-05");
			if($yperiod != date("Y", strtotime("-1 month", strtotime($ppay))))
			{
				$end_cutoff = date("$yperiod-$mperiod-05");
			}
			$data['CutOff']['End'] = $end_cutoff;
		}
		else if($pperiod == '30' || $pperiod == '28')
		{
			$start_cutoff = date("$yperiod-$mperiod-06");
			$data['CutOff']['Start'] = $start_cutoff;			
			$end_cutoff = date("$yperiod-$mperiod-20");			
			if($yperiod != date("Y", strtotime("-1 month", strtotime($ppay))))
			{
				$end_cutoff = date("$yperiod-$mperiod-20");
			}			
			$data['CutOff']['End'] = $end_cutoff;
		}

		$data['EmployeeNumber'] = $empid;
		$data['Cutoff'] = $ppay;
		$data['Period'] = $start_cutoff." - ".$end_cutoff;

		// GET AGENT SCHEDULE START
		$sqlStatement =  "SELECT DISTINCT shift_date, schedule, non_ops_hrs FROM dtr.agent_sched 
							WHERE emp_id = :empid AND shift_date BETWEEN :start AND :end 
						   	ORDER BY shift_date ASC";

		$sqlParameters = array(':empid' => $empid, ':start' => $start_cutoff, ':end' => $end_cutoff);
		$sqlQuery = $db->prepare($sqlStatement);
		$sqlQuery->execute($sqlParameters);	
		// GET AGENT SCHEDULE END

		// GENERATE PERIOD DATES START
		$periodDates = new DatePeriod(
						     new DateTime($start_cutoff),
						     new DateInterval('P1D'),
						     new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($end_cutoff))))
						);
		foreach( $periodDates as $periodDate) { $periodDatesArray[] = $periodDate->format('Y-m-d'); }
		// GENERATE PERIOD DATES END
		//dump($periodDatesArray);
		//$periodDatesArray = ['2017-05-01'];
		//dump($periodDatesArray);
		//exit;
		// GET HOLIDAYS START
		$getHolidaysStatement = "SELECT holiday, hshift, hstart, hend, htype FROM dtr.holidays 
								WHERE hshift IN(" . str_repeat('?,', count($periodDatesArray)-1) . " ?)";
		$getHolidaysQuery = $db->prepare($getHolidaysStatement);
		$getHolidaysQuery->execute($periodDatesArray);	
		$HolidayDates = collect($getHolidaysQuery->fetchAll(PDO::FETCH_ASSOC));
		// GET HOLIDAYS END

		// GET AGENT LILO START
		$getLilosStatement = "SELECT shift_date, login_date, login_time, logout_date, logout_time FROM dtr.agent_lilo 
								WHERE shift_date IN(" . str_repeat('?, ', count($periodDatesArray)-1) . "?)	AND empid = ?
								ORDER BY login_date ASC, login_time ASC";
		$getLilosQuery = $db->prepare($getLilosStatement);
		$periodDatesArray[] = $empid;
		$getLilosQuery->execute($periodDatesArray);	
		$AgentLILOs = collect($getLilosQuery->fetchAll(PDO::FETCH_ASSOC));
		// GET AGENT LILO END

		if($sqlQuery->rowCount() !=0)
		{
			$ashift=array();
			$s=0;
			$total_hrs = 0;	
			$total_excess = 0;
			$total_otexcess = 0;	
			$total_preotexcess = 0;
			$total_staffedhrs_excess = 0;
			$total_overAux = 0;	
			$total_reghrs = 0;
			$total_lholiday = 0;
			$total_sholiday = 0;	
			$ot_reghrs = 0;
			$ot_lholiday = 0;
			$ot_sholiday = 0;	
			$rdot_reghrs = 0;
			$rdot_lholiday = 0;
			$rdot_sholiday = 0;			
			$nd_reghrs = 0;
			$nd_lholiday = 0;
			$nd_sholiday = 0;	
			$otnd_reghrs = 0;
			$otnd_lholiday = 0;
			$otnd_sholiday = 0;	
			$rdnd_reghrs = 0;
			$rdnd_lholiday = 0;
			$rdnd_sholiday = 0;
			
			while($row1 = $sqlQuery->fetch(PDO::FETCH_ASSOC))	
			{
				$ashift[$s]= $row1['shift_date'];
				$dexcess = 0;
				$pLo = 0;
				$breakTime = 0;
				$lunchTime = 0;
				$dinterval = 0;
				$dotexcess = 0;
				$dpreotexcess = 0;
				$dpreot = 0;
				$daily_excess_staffedhrs = 0;
				$daily_overAux = 0;	

				$daily_reghrs = 0;
				$daily_lholiday = 0;
				$daily_sholiday = 0;	

				$daily_ot_reghrs = 0;
				$daily_ot_lholiday = 0;
				$daily_ot_sholiday = 0;	

				$daily_rdot_reghrs = 0;
				$daily_rdot_lholiday = 0;
				$daily_rdot_sholiday = 0;	

				$daily_nd_reghrs = 0;
				$daily_nd_lholiday = 0;
				$daily_nd_sholiday = 0;	

				$daily_otnd_reghrs = 0;
				$daily_otnd_lholiday = 0;
				$daily_otnd_sholiday = 0;	

				$daily_rdnd_reghrs = 0;
				$daily_rdnd_lholiday = 0;
				$daily_rdnd_sholiday = 0;	

				$theYear = date("Y",strtotime($row1['shift_date']));

				// DETERMINE DST START
				$DSTdate = new DateTime($row1['shift_date'].' America/New_York');
				if($DSTdate->format('I') == 1)
				{
					$dst = 1; $timeZone = 'DST';
				}
				else
				{
					$dst = 0; $timeZone = 'EST';
				}
				// DETERMINE DST END

				$row4 = $row1;

				$daily_reg_hrs = 0;
				$daily_night_diff_hrs = 0;
				$daily_pre_shift_ot = 0;
				$daily_night_diff_pre_shift_ot_hrs = 0;
				$daily_post_shift_ot = 0;
				$daily_night_diff_post_shift_ot_hrs = 0;
				$daily_rdot = 0;
				$daily_night_diff_rdot = 0;

				$daily_legal_holiday = 0;
				$daily_lhnd = 0;
				$daily_lhpreot  = 0;
				$daily_lhndpreot = 0;
				$daily_lhot = 0;
				$daily_lhndot = 0;
				$daily_lhrdot = 0;
				$daily_lhndrdot = 0;

				$daily_special_holiday = 0;
				$daily_shnd = 0;
				$daily_shpreot = 0;
				$daily_shndpreot = 0;
				$daily_shot = 0;
				$daily_shndot = 0;
				$daily_shrdot = 0;
				$daily_shndrdot = 0;

				$data['Records'][date('Ymd',strtotime($row1['shift_date']))]['Schedule'] = $row4['schedule'];
			 	$data_shift_date = date('Ymd',strtotime($row1['shift_date']));

			 	$holidayArray = [];

			 	$shiftDate = $row1['shift_date'];

			 	// FILTER AGENT LILO FROM SHIFT DATE START
		 		$AgentLILO = $AgentLILOs->filter(function($value, $key) use($shiftDate){
					return ($value['shift_date'] == $shiftDate) ? $value : '';
				});
				// FILTER AGENT LILO FROM SHIFT DATE END
		 		
		 		// CALCULATE DTR BREAKDOWN
				foreach ($AgentLILO as $row5) 
				{
$rdotSched = array("off","mloa","ps","s","lmat","bl","vl","sl","birthday","lpat","br","el","-");
	$interval = 0;
	$ot = 0;
	$nd = 0;
	$nd_ot = 0;
	$rdot = 0;
	$nd_rdot = 0;			
	$preot = 0;
	$nd_preot = 0;
	$LHinterval = 0;
	$OHinterval = 0;
	$special_holiday = 0;
	$legal_holiday = 0;
	$shnd = 0;
	$lhnd = 0;			
	$hot = 0;
	$hndot = 0;
	$lhpreot = 0;
	$lhndpreot = 0;
	$lhot = 0;
	$lhndot = 0;
	$lhrdot = 0;
	$lhndrdot = 0;			
	$shpreot = 0;
	$shndpreot = 0;
	$shot = 0;
	$shndot = 0;
	$shrdot = 0;
	$shndrdot = 0;			
	$excess = 0;	
	//Initialize
	$hpreot = 0;
	$hndpreot = 0;
	$OHpreot = 0;
	$OHpreot_nd = 0;	
	$Ohnd_start = strtotime($row5['shift_date'] . "10:00:00 +12 hours");
	$Ohnd_stop = strtotime($row5['shift_date'] . "18:00:00 +12 hours");
	$dstAdd = ($dst == '1') ? "+13 hours" : "+12 hours";

					// if($row4['non_ops_hrs'] == 1)
					// {
					// 	$dstAdd = '';
					// }
					//ACTUAL HOURS (FOR DISPLAY)
					$acdstart = date("Y-m-d H:i:s",strtotime($row5['login_date'].$row5['login_time'].$dstAdd));
					$acdend = date("Y-m-d H:i:s",strtotime($row5['logout_date'].$row5['logout_time'].$dstAdd));
						
					//CONVERT DISPLAY TO MANILA TIME 
					$sched = explode("-", $row4['schedule'], 2);
					if(count($sched)<=1)
					{
						$sched_start = $sched_stop = $row4['schedule'];
					}
					else
					{
						$sched_start = date("H:i:s",strtotime($sched[0]));
						$sched_stop = date("H:i:s", strtotime($sched[1]));
					}
						
					$start_date = date("Y-m-d", strtotime($row5['shift_date']));
					$stop_date = date("Y-m-d", strtotime($row5['shift_date']));
					
					$dsched_start = date("Y-m-d H:i:s", strtotime($start_date.$sched_start.$dstAdd));
					$dsched_stop = date("Y-m-d H:i:s", strtotime($stop_date.$sched_stop.$dstAdd));

					if(strtotime($dsched_stop) <= strtotime($dsched_start))
					{
						$dsched_stop = date("Y-m-d H:i:s", strtotime($dsched_stop."+1 day"));

						if(strtotime($dsched_stop) <= strtotime($dsched_start))
						{
							$dsched_stop = date("Y-m-d H:i:s", strtotime($dsched_stop."+1 day"));
						}
					}
					/**end edit**/
						
					$dsched = $dsched_start." to ".$dsched_stop." ";
					$dshift = $row5['shift_date'];
					
					$dstart = date("Y-m-d H:i:s",strtotime($row5['login_time'].$row5['login_date'].$dstAdd));
					$dend = date("Y-m-d H:i:s",strtotime($row5['logout_time'].$row5['logout_date'].$dstAdd));
					// if(strtotime($row4['shift_date']) == strtotime('2017-04-30'))
					// {
					// 	dump(['start' => $dsched_start, 'end' => $dsched_stop]);
					// 	dump($row5['login_time'].$row5['login_date'].$dstAdd);
					// 	dump(strtotime($row5['login_time'].$row5['login_date'].$dstAdd));
					// 	dump($row5['login_date'].$row5['login_time'].$dstAdd);
					// 	dump(strtotime($row5['login_date'].$row5['login_time'].$dstAdd));
					// 	dump($row5['login_date'].' '.$row5['login_time'].' '.$dstAdd);
					// 	dump(strtotime($row5['login_date'].' '.$row5['login_time'].' '.$dstAdd));
					// 	dump(['start' => $dstart, 'end' => $dend]);
					// 	dump((strtotime($dend) - strtotime($dstart))/3600);
					// 	exit;
					// }
				
					//DECLARE NIGHT DIFF
					$nd_start = strtotime($row5['shift_date'] . "22:00:00");
					$nd_stop = strtotime($row5['shift_date'] . "06:00:00 +1 day");
					
					// DECLARE BEFORE AND AFTER OPS 
					$bOps = ($dst == '1') ? strtotime($row5['shift_date'] . "09:00:00 +12 hours") : strtotime($row5['shift_date'] . "08:00:00 +12 hours");
					$aOps = ($dst == '1') ? strtotime($row5['shift_date'] . "13:00:00 +24 hours") : strtotime($row5['shift_date'] . "12:00:00 +24 hours");

					// IF NON OPS HOURS
					if($row4['non_ops_hrs'] != 1)
					{
						// SCRUB LOGIN (BEFORE 8PM) AND/OR LOGOUT (AFTER 12NN) [MNL]
						if( (strtotime($dstart) <= $bOps) && (strtotime($dend) >=  $bOps) )
						{
							$excess = ($bOps - strtotime($dstart))/3600;
							$dstart = date("Y-m-d H:i:s",$bOps);
							if(strtotime($dend) <= $bOps)
							{
								$dend = date("Y-m-d H:i:s", $bOps);
							}
						}

						if( (strtotime($dstart) <=  $aOps) && (strtotime($dend) >=  $aOps) )
						{
							$excess = (strtotime($dend) - $aOps)/3600;
							$dend = date("Y-m-d H:i:s", $aOps);
						}
					}
					
					// DETERMINE HRS
					//$pLo = strtotime($dend);

					$interval = (strtotime($dend) - strtotime($dstart))/3600;
					//dd([strtotime($dstart), $pLo]);
					if(strtotime($dstart) < $pLo && $pLo !=0)
					{
						//dd($interval);
						$interval = $interval - ($pLo - strtotime($dstart))/3600;
					}

					if($interval <= 0)
					{
						$interval = 0;
					}

					if($row4['non_ops_hrs'] != 1)
					{
						if( ( (strtotime($dstart) < $bOps) && (strtotime($dend) < $bOps) ) || ( (strtotime($dstart) > $aOps) && (strtotime($dend) > $aOps) ) )
						{
							$excess = $interval;
							$interval = 0;
						}
					}
					
					if( !in_array(strtolower($row4['schedule']),$rdotSched) )
					{
					//DETERMINE OT (POSTSHIFT)
						if(strtotime($dstart) > strtotime($dsched_stop) && strtotime($dend) > strtotime($dsched_stop))
						{
							$ot = $interval;
							$interval = 0;
						}
						else if(strtotime($dstart) <= strtotime($dsched_stop) && strtotime($dend) > strtotime($dsched_stop))
						{
							$ot = round((strtotime($dend) - strtotime($dsched_stop))/3600,2);
							$interval = $interval - $ot;
						}
						
					//DETERMINE OT (PRESHIFT)
						if(strtotime($dstart) < strtotime($dsched_start) && strtotime($dend) < strtotime($dsched_start))
						{
							$preot = $interval;
							$interval = 0;
						}
						else if(strtotime($dend) >= strtotime($dsched_start) && strtotime($dstart) < strtotime($dsched_start))
						{
							$preot = round((strtotime($dsched_start) - strtotime($dstart))/3600,2);
							$interval = $interval - $preot;
						}
						
					}
					else
					{
						$preot = 0;
						$nd_preot = 0;
						$ot = 0;
						$nd_ot = 0;
					}
					
					//SCHEDULED NIGHT DIFF
					if(in_array(strtolower($row4['schedule']),$rdotSched))
					{
						$snd_start = $nd_start;
						$snd_stop = $nd_stop;
					}
					else
					{
						if(strtotime($dsched_start)<=$nd_start)
						{
							$snd_start = $nd_start;
						}
						else
						{
							$snd_start = strtotime($dsched_start);
						}

						if(strtotime($dsched_stop)>=$nd_stop)
						{
							$snd_stop = $nd_stop;
						}
						else
						{
							$snd_stop = strtotime($dsched_stop);
						}
					}

					$nd =  (($snd_stop - $snd_start)/3600);

					if(strtotime($dstart) > $snd_start)
					{
						$nd = $nd - ((strtotime($dstart) - $snd_start)/3600);
					}

					if(strtotime($dend) < $snd_stop)
					{
						$nd = $nd - (($snd_stop - strtotime($dend))/3600);
					}

					if ($nd <= 0)
					{
						$nd = 0;
					}

					if(($interval <= 0)&&($nd >= $interval))
					{
							$nd = 0;
					}

					//NIGHT DIFF (POST-OT)
					if(strtotime($dend)>strtotime($dsched_stop) && strtotime($dsched_stop) < $nd_stop)
					{
						$nd_ot = $ot;
						if(strtotime($dend)>$nd_stop)
						{
							$nd_ot = $ot - (strtotime($dend) - $nd_stop)/3600;					
						}
					}
					
					//NIGHT DIFF (PRE-OT)
					if(strtotime($dsched_start)>$nd_start && strtotime($dstart) < strtotime($dsched_start))
					{
						$nd_preot = $preot;
						if(strtotime($dstart)<$nd_start)
						{
							$nd_preot = $preot - ($nd_start - strtotime($dstart))/3600;
						}
					}
					
					if($ot <= 0)
					{
						$nd_ot = 0;
					}

					if($nd_ot <= 0)
					{
						$nd_ot = 0;
					}

					if($preot <= 0)
					{ 
						$nd_preot = 0;
					}

					if($nd_preot <= 0) 
					{
						$nd_preot = 0;
					}

					//OPEN OT DATES
					$open_ot_dates = array(
										strtotime('2016-12-25'),
										strtotime('2016-12-26'),
										strtotime('2016-12-27'),
										strtotime('2016-12-28'),
										strtotime('2016-12-29'),
										strtotime('2016-12-30'),
										strtotime('2016-12-31'),
										strtotime('2017-01-01')
									);

					// SCRUB PRE-OT HOURS (MAX 5 HOURS TOTAL PER SHIFT)
					if($row4['non_ops_hrs'] != 1 && !in_array(strtotime($row1['shift_date']), $open_ot_dates))
					{	
						if(($dpreot + $preot + $lhpreot + $shpreot)>5){
							$dumPreot = $dpreot + $preot + $lhpreot + $shpreot;
							$preot -= ($dumPreot - 5);

							$dpreotexcess += $dumPreot - 5;
						}
					}
					//SCRUB OT HOURS (STAFFED HOURS MAX 13)
					$dumInterval = $dinterval + $interval + $preot + $ot + $lhot + $shot;
					if($dumInterval>13){
						if($row4['non_ops_hrs'] != 1 && !in_array(strtotime($row4['shift_date']), $open_ot_dates))
						{
							$ot -= ($dumInterval - 13);
							if($ot<0){
								#$interval += $ot;
								$ot = 0;
							}
							$daily_excess_staffedhrs = $dumInterval - 13;
						}
					}
					
					//RDOT
					if(in_array(strtolower($row4['schedule']), $rdotSched))
					{
						$rdot = $interval + $ot + $preot;
						$nd_rdot = $nd + $nd_ot + $nd_preot;
						$interval = 0;
						$preot = 0;
						$ot = 0;
						$nd = 0;
						$nd_ot = 0;
						$nd_preot = 0;
					}

				//HOLIDAY HOURS
				$loginDate = $row5['login_date'];
				$Lholiday = [];
				$Lholiday = $HolidayDates->filter(function($value, $key) use($loginDate){
					return ($value['hshift'] == $loginDate) ? $value : '';
				});
				$Lholiday = (count($Lholiday) <= 0) ? ['htype' => '', 'hshift' => '', 'hstart' => '', 'hend' => '', 'holiday' => ''] : $Lholiday->first();

				$logoutDate = $row5['logout_date'];
				$Oholiday = [];
				$Oholiday = $HolidayDates->filter(function($value, $key) use($logoutDate){
					return ($value['hshift'] == $logoutDate) ? $value : '';
				});
				$Oholiday = (count($Oholiday) <= 0) ? ['htype' => '', 'hshift' => '', 'hstart' => '', 'hend' => '', 'holiday' => ''] : $Oholiday->first();

				$Ohnd_stop = strtotime($row5['shift_date'] . "18:00:00 +12 hours");

					if ((strtotime($loginDate) == strtotime($logoutDate)) || ($Lholiday['htype'] == $Oholiday['htype'])){
					#if ($Sday == $Eday){
						if ($Lholiday['htype'] != ''){ //echo "Holiday found! ".$Lholiday['holiday'];
							if($Lholiday['htype'] == "Special Holiday"){
								$special_holiday = $interval;
								$shnd = $nd;
								$shpreot = $preot;
								$shndpreot = $nd_preot;
								$shot = $ot;
								$shndot = $nd_ot;
								$shrdot = $rdot;
								$shndrdot = $nd_rdot;
								$holidayArray[] = 'Special';
								
								if( in_array(strtolower($row4['schedule']),$rdotSched) ){
									$rdot = $rdot - $shrdot;
									$nd_rdot = $nd_rdot - $shndrdot;
								}
							}
							else if ($Lholiday['htype'] == "Regular Holiday"){
								$legal_holiday = $interval;
								$lhnd = $nd;
								$lhpreot = $preot;
								$lhndpreot = $nd_preot;
								$lhot = $ot;
								$lhndot = $nd_ot;
								$lhrdot = $rdot;
								$lhndrdot = $nd_rdot;
								$holidayArray[] = 'Legal';
								
								if( in_array(strtolower($row4['schedule']),$rdotSched) ){
									$rdot = $rdot - $lhrdot;
									$nd_rdot = $nd_rdot - $lhndrdot;
								}
							}
						}

						$ot = $ot - ($lhot + $shot);
						if($ot <= 0){
							$ot = 0;
							$nd_ot = 0;
						}
						
						$preot = $preot - ($lhpreot + $shpreot);
						if($preot <= 0){
							$preot = 0;
							$nd_preot = 0;
						}
					}
					else{
						if ($Lholiday['htype'] != ''){ //echo "Login holiday found! ".$Lholiday['holiday'];
							if(strtotime($dstart) < $pLo && $pLo !=0){
							// NEW EDIT
								$LHinterval = $LHinterval - (strtotime($Lholiday['hend']) - strtotime($dstart))/3600;
							}
							
							if(strtotime($dstart)>$Ohnd_start){
								$hND = $LHinterval;
							}
							else if(strtotime($dstart)<$Ohnd_start){
								$hND = $LHinterval - ($Ohnd_start - strtotime($dstart))/3600;
							}
							else if(strtotime($dstart)==$Ohnd_stop){
								$hND = 0;
							}
							else $hND = 0;
							
							if ($hND <= 0) $hND = 0;
							
							if(strtotime($dstart) < strtotime($dsched_start)){
								if(strtotime($dsched_start) > strtotime($Lholiday['hend'])){
									$hpreot = $LHinterval;
									$hndpreot = $hND;

									$nd_preot = $nd_preot - $hND;
								}
								else{
									$hpreot = $preot;
									$hndpreot = $nd_preot;
									$preot = 0;
									$nd_preot = 0;
								}

								$LHinterval = $LHinterval - $hpreot;
								$hND = $hND - $hndpreot;
							}
							
							if($Lholiday['htype'] == "Special Holiday"){
								$special_holiday = $LHinterval;
								$shnd = $hND;
								$shpreot = $hpreot;
								$shndpreot = $hndpreot;
								
								//RDOT
								if( in_array(strtolower($row4['schedule']),$rdotSched) ){
									$shrdot = $special_holiday + $shot;
									$shndrdot = $shnd;
									$special_holiday = 0;
									$shot = 0;
									$shnd = 0;
									
									$rdot = $rdot - $shrdot;
									$nd_rdot = $nd_rdot - $shndrdot;
								}
								$holidayArray[] = 'Special';
							}
							
							else if ($Lholiday['htype'] == "Regular Holiday"){
								$legal_holiday = $LHinterval;
								$lhnd = $hND;
								$lhpreot = $hpreot;
								$lhndpreot = $hndpreot;
								
								//RDOT
								if( in_array(strtolower($row4['schedule']),$rdotSched) ){
									$lhrdot = $legal_holiday + $lhot;
									$lhndrdot = $lhnd;
									$legal_holiday = 0;
									$lhot = 0;
									$lhnd = 0;
									
									$rdot = $rdot - $lhrdot;
									$nd_rdot = $nd_rdot - $lhndrdot;
								}
								$holidayArray[] = 'Legal';
							}
						}
						if ($Oholiday['htype'] != '')
						{ #echo "Logout holiday found! ".$Oholiday['holiday'];
							if(strtotime($dstart) < $pLo && $pLo !=0){
								// NEW EDIT
								$OHinterval = $OHinterval - (strtotime($dend) - strtotime($Oholiday['hstart']))/3600;
							}
							if(strtotime($dend)<$Ohnd_stop)
							{
								$hND = $OHinterval;
							}
							else if(strtotime($dend)>=$Ohnd_stop)
							{
								$hND = ($Ohnd_stop - strtotime($Oholiday['hstart']))/3600;
							}
							else if(strtotime($dstart)==$Ohnd_stop)
							{
								$hND = 0;
							}
							
							if ($hND <= 0) $hND = 0;

							if(strtotime($dend) > strtotime($dsched_stop))
							{
								$hot = $ot;
								$hndot = $nd_ot;
								$ot = 0;
								$nd_ot = 0;
								$OHinterval = $OHinterval - $hot;
								$hND = $hND - $hndot;
							}

							if($preot>0 && ((strtotime($dsched_start) > strtotime($Oholiday['hstart']))))
							{
								$OHpreot = (strtotime($dsched_start) - strtotime($Oholiday['hstart']))/3600;
								$OHinterval = $OHinterval - $OHpreot;
							
								if($nd_preot)
								{
									$OHpreot_nd = $OHpreot;
									$hND -= $OHpreot_nd;
									$nd_preot -= $OHpreot_nd;
								}

							}
							
							if($Oholiday['htype'] == "Special Holiday")
							{
								$special_holiday = $OHinterval;
								$shnd = $hND;
								$shot = $hot;
								$shndot = $hndot;
								$shpreot = $OHpreot;
								$shndpreot = $OHpreot_nd;
								
								//RDOT
								if( in_array(strtolower($row4['schedule']),$rdotSched) )
								{
									$shrdot = $special_holiday + $shot;
									$shndrdot = $shnd;
									$special_holiday = 0;
									$shot = 0;
									$shnd = 0;									
									$rdot = $rdot - $shrdot;
									$nd_rdot = $nd_rdot - $shndrdot;
								}
								
								$holidayArray[] = 'Special';
							}
							else if ($Oholiday['htype'] == "Regular Holiday")
							{
								$legal_holiday = $OHinterval;
								$lhnd = $hND;
								$lhot = $hot;
								$lhndot = $hndot;
								$lhpreot = $OHpreot;
								$lhndpreot = $OHpreot_nd;
								
								//RDOT
								if( in_array(strtolower($row4['schedule']),$rdotSched) )
								{
									$lhrdot = $legal_holiday + $lhot;
									$lhndrdot = $lhnd;
									$legal_holiday = 0;
									$lhot = 0;
									$lhnd = 0;
									$rdot = $rdot - $lhrdot;
									$nd_rdot = $nd_rdot - $lhndrdot;
								}		
								
								$holidayArray[] = 'Legal';
							}
						}
						
						// holiday 3 ???

						$preot = $preot - ($lhpreot + $shpreot);
						if($preot <= 0) $preot = 0;
						
						$ot = $ot - ($lhot + $shot);
						if($ot <= 0) $ot = 0;
					}

					if($legal_holiday<0)
					{
						$legal_holiday = 0;
					}
					if($special_holiday<0)
					{
						$special_holiday = 0;
					}
					if($lhnd<0)
					{
						$lhnd = 0;
					}
					if($shnd<0)
					{
						$shnd = 0;
					}

					$interval = $interval - ($legal_holiday + $special_holiday);

					#If ND > HRS COUNTERPART
					if($nd>$interval)
					{
						$nd = $nd -  ($nd - $interval);
					}
					if($nd_ot>$ot)
					{
						$nd_ot = $nd_ot -  ($nd_ot - $ot);
					}
					if($nd_preot>$preot)
					{
						$nd_preot = $nd_preot -  ($nd_preot - $preot);
					}
					if($nd_rdot>$rdot)
					{
						$nd_rdot = $nd_rdot -  ($nd_rdot - $rdot);
					}

					if($lhnd>$legal_holiday)
					{
						$lhnd = $lhnd -  ($lhnd - $legal_holiday);
					}
					if($shnd>$special_holiday)
					{
						$shnd = $shnd -  ($shnd - $special_holiday);
					}

					if($lhndpreot>$lhpreot)
					{
						$lhndpreot = $lhndpreot -  ($lhndpreot - $lhpreot);
					}
					if($lhndot>$lhot)
					{
						$lhndot = $lhndot -  ($lhndot - $lhot);
					}
					if($lhndrdot>$lhrdot)
					{
						$lhndrdot = $lhndrdot -  ($lhndrdot - $lhrdot);
					}

					if($shndpreot>$shpreot)
					{
						$shndpreot = $shndpreot -  ($shndpreot - $shpreot);
					}
					if($shndot>$shot)
					{
						$shndot = $shndot -  ($shndot - $shot);
					}
					if($shndrdot>$shrdot)
					{
						$shndrdot = $shndrdot -  ($shndrdot - $shrdot);
					}

					if($interval <= 0) $interval = 0;
					$nd = $nd - ($lhnd + $shnd);
					if($nd <= 0) $nd = 0;
					if($nd_preot <=0) $nd_preot = 0;
					
					if($rdot <= 0) $rdot = 0;

					//SET PREVIOUS LOGOUT
					if($pLo < strtotime($dend))
					{
						$pLo = strtotime($dend);
					}

					//SET PREVIOUS HIGHEST LOGOUT
					// if( !(strtotime($dend) < $pLo) )
					// {
					// 	$pLo = strtotime($dend);
					// }
					
					//Daily TOTALS
					$dexcess += $excess;

					$dinterval += $interval + $preot + $ot + $rdot + $special_holiday + $legal_holiday + $lhpreot + $shpreot + $lhot + $shot + $lhrdot + $shrdot;		

					$dpreot += $preot + $lhpreot + $shpreot;

					$daily_reghrs 				+=	$interval;
					$daily_lholiday 			+=	$legal_holiday;
					$daily_sholiday 			+=	$special_holiday;	

					$daily_ot_reghrs 			+=	$preot + $ot;
					$daily_ot_lholiday			+=	$lhpreot + $lhot;
					$daily_ot_sholiday			+=	$shpreot + $shot;					
					$daily_rdot_reghrs			+=	$rdot;
					$daily_rdot_lholiday		+=	$lhrdot;
					$daily_rdot_sholiday		+=	$shrdot;

					$daily_nd_reghrs			+=	$nd;
					$daily_nd_lholiday			+=	$lhnd;
					$daily_nd_sholiday			+=	$shnd;					
					$daily_otnd_reghrs			+=	$nd_preot + $nd_ot;
					$daily_otnd_lholiday		+=	$lhndpreot + $lhndot;
					$daily_otnd_sholiday		+=	$shndpreot + $shndot;					
					$daily_rdnd_reghrs			+=	$nd_rdot;
					$daily_rdnd_lholiday		+=	$lhndrdot;
					$daily_rdnd_sholiday		+=	$shndrdot;

					// get daily breakdown
					$daily_reg_hrs						+=	$interval;
					$daily_night_diff_hrs				+=	$nd;
					$daily_pre_shift_ot					+=	$preot;
					$daily_night_diff_pre_shift_ot_hrs	+=	$nd_preot;
					$daily_post_shift_ot				+=	$ot;
					$daily_night_diff_post_shift_ot_hrs	+=	$nd_ot;
					$daily_rdot							+=	$rdot;
					$daily_night_diff_rdot				+=	$nd_rdot;

					$daily_legal_holiday				+=	$legal_holiday;
					$daily_lhnd							+=	$lhnd;
					$daily_lhpreot						+=	$lhpreot;
					$daily_lhndpreot					+=	$lhndpreot;
					$daily_lhot							+=	$lhot;
					$daily_lhndot						+=	$lhndot;
					$daily_lhrdot						+=	$lhrdot;
					$daily_lhndrdot						+=	$lhndrdot;

					$daily_special_holiday				+=	$special_holiday;
					$daily_shnd							+=	$shnd;
					$daily_shpreot						+=	$shpreot;
					$daily_shndpreot					+=	$shndpreot;
					$daily_shot							+=	$shot;
					$daily_shndot						+=	$shndot;
					$daily_shrdot						+=	$shrdot;
					$daily_shndrdot						+=	$shndrdot;
					$s++;
				}//end foreach

				#REMOVE ONE HOUR OT SCRUB PP2014-06-30
				//Scrub Daily OT -- must be more than 1 hour
				if(($daily_ot_reghrs + $daily_ot_lholiday + $daily_ot_sholiday)<0.999999999)
				{
					$dotexcess = $daily_ot_reghrs + $daily_ot_lholiday + $daily_ot_sholiday;
					$daily_ot_reghrs = 0;
					$daily_ot_lholiday = 0;
					$daily_ot_sholiday = 0;
					$daily_otnd_reghrs = 0;
					$daily_otnd_lholiday = 0;
					$daily_otnd_sholiday = 0;
					$dinterval -= $dotexcess;
				}

				$daily_excess_staffedhrs += $dpreotexcess;

				//Difference
				if($lunchTime != 0 && $lunchTime>1500)
				{
					$daily_overAux += ($lunchTime-1500)/3600; if($daily_overAux<0){ $daily_overAux = 0; }
				}
				if($breakTime != 0 && $breakTime>1200)
				{
					$daily_overAux += ($breakTime-1200)/3600; if($daily_overAux<0){ $daily_overAux = 0; }
				}

				if($daily_reghrs==0 && $daily_rdot_reghrs>0)
				{
					$daily_rdot_reghrs -= $daily_overAux;	
					if($daily_rdot_reghrs<0)
					{ 
						$daily_rdot_reghrs = 0; 
					}
					else
					{
						$dinterval -= $daily_overAux;
					}
				}
				else
				{
					$daily_reghrs -= $daily_overAux;	
					if($daily_reghrs<0)
					{ 
						$daily_reghrs = 0; 
					}
					else
					{
						$dinterval -= $daily_overAux;
					}
				}


				if($daily_nd_reghrs>$daily_reghrs)
				{
					$daily_nd_reghrs = $daily_nd_reghrs -  ($daily_nd_reghrs - $daily_reghrs);
				}
				#If ND > OT COUNTERPART
				if($daily_otnd_reghrs>$daily_ot_reghrs)
				{
					$daily_otnd_reghrs = $daily_otnd_reghrs -  ($daily_otnd_reghrs - $daily_ot_reghrs);
				}
				if($daily_otnd_lholiday>$daily_ot_lholiday)
				{
					$daily_otnd_lholiday = $daily_otnd_lholiday -  ($daily_otnd_lholiday - $daily_ot_lholiday);
				}
				if($daily_otnd_sholiday>$daily_ot_sholiday)
				{
					$daily_otnd_sholiday = $daily_otnd_sholiday -  ($daily_otnd_sholiday - $daily_ot_sholiday);
				}

				$data['Records'][$data_shift_date]['HolidayTag'] = array_unique($holidayArray);
				$data['Records'][$data_shift_date]['ShiftDate'] = date("Y-m-d", strtotime($data_shift_date));

				//Regular
				$data['Records'][$data_shift_date]['Regular']['Hours'] = number_format($daily_reg_hrs,2);
				$data['Records'][$data_shift_date]['Regular']['NDHours'] = number_format($daily_night_diff_hrs,2);
				$data['Records'][$data_shift_date]['Regular']['Overtime']['Preshift']['Hours'] = number_format($daily_pre_shift_ot,2);
				$data['Records'][$data_shift_date]['Regular']['Overtime']['Preshift']['NDHours'] = number_format($daily_night_diff_pre_shift_ot_hrs,2);
				$data['Records'][$data_shift_date]['Regular']['Overtime']['Postshift']['Hours'] = number_format($daily_post_shift_ot,2);
				$data['Records'][$data_shift_date]['Regular']['Overtime']['Postshift']['NDHours'] = number_format($daily_night_diff_post_shift_ot_hrs,2);
				$data['Records'][$data_shift_date]['Regular']['Overtime']['Restday']['Hours'] = number_format($daily_rdot,2);
				$data['Records'][$data_shift_date]['Regular']['Overtime']['Restday']['NDHours'] = number_format($daily_night_diff_rdot,2);
				//Legal
				$data['Records'][$data_shift_date]['Legal']['Hours'] = number_format($daily_legal_holiday,2);
				$data['Records'][$data_shift_date]['Legal']['NDHours'] = number_format($daily_nd_lholiday,2);
				$data['Records'][$data_shift_date]['Legal']['Overtime']['Preshift']['Hours'] = number_format($daily_lhpreot,2);
				$data['Records'][$data_shift_date]['Legal']['Overtime']['Preshift']['NDHours'] = number_format($daily_lhndpreot,2);
				$data['Records'][$data_shift_date]['Legal']['Overtime']['Postshift']['Hours'] = number_format($daily_lhot,2);
				$data['Records'][$data_shift_date]['Legal']['Overtime']['Postshift']['NDHours'] = number_format($daily_lhndot,2);
				$data['Records'][$data_shift_date]['Legal']['Overtime']['Restday']['Hours'] = number_format($daily_lhrdot,2);
				$data['Records'][$data_shift_date]['Legal']['Overtime']['Restday']['NDHours'] = number_format($daily_lhndrdot,2);
				//Special
				$data['Records'][$data_shift_date]['Special']['Hours'] = number_format($daily_special_holiday,2);
				$data['Records'][$data_shift_date]['Special']['NDHours'] = number_format($daily_shnd,2);
				$data['Records'][$data_shift_date]['Special']['Overtime']['Preshift']['Hours'] = number_format($daily_shpreot,2);
				$data['Records'][$data_shift_date]['Special']['Overtime']['Preshift']['NDHours'] = number_format($daily_shndpreot,2);
				$data['Records'][$data_shift_date]['Special']['Overtime']['Postshift']['Hours'] = number_format($daily_shot,2);
				$data['Records'][$data_shift_date]['Special']['Overtime']['Postshift']['NDHours'] = number_format($daily_shndot,2);
				$data['Records'][$data_shift_date]['Special']['Overtime']['Restday']['Hours'] = number_format($daily_shrdot,2);
				$data['Records'][$data_shift_date]['Special']['Overtime']['Restday']['NDHours'] = number_format($daily_shndrdot,2);
				//Excess
				$data['Records'][$data_shift_date]['Excess']['ExcessHours'] = number_format($daily_excess_staffedhrs,2);
				$data['Records'][$data_shift_date]['Excess']['Regular'] = number_format($dexcess,2);
				$data['Records'][$data_shift_date]['Excess']['Overtime'] = number_format($dotexcess,2);
				$data['Records'][$data_shift_date]['Excess']['Overaux'] = number_format($daily_overAux,2);
				//Total
				$data['Records'][$data_shift_date]['Total']['StaffedHours'] = number_format($dinterval,2);
				$data['Records'][$data_shift_date]['Total']['ExcessHours'] = number_format($total_excess,2);

				// $seconds = (60*60)*24;
				// return response()->json($data)
				// 			 ->header("Cache-Control", "private, max-age=$seconds")
  		// 					 ->header("Expires", gmdate('r', time()+$seconds))
				// 			 ->setCallback( $request->callback );

				// exit;
			}		

			//return response()->json($data)->setCallback( $request->callback );

			$seconds = (60*60)*24;

			return response()->json($data)
							 ->header("Cache-Control", "private, max-age=$seconds")
  							 ->header("Expires", gmdate('r', time()+$seconds))
							 ->setCallback( $request->callback );			
 		}
		else 
		{
			$data['Error']['Message'] = "Error Message!!!";
			return response()->json($data)->setCallback( $request->callback );
		}// END of else of if(mysql_num_rows($sql1)!=0) //end condition

		
    }// END of Public function getDtr()

	public function getAgentOnLilo(Request $request)
	{
		$employee_list = AgentDtrLilo::where('shift_date','=', $request->shift_date)
									->groupBy('empid')
									->lists('empid');
		$count = count($employee_list);
		$shift_date = date("Y-m-d", strtotime($request->shift_date));
		$data = compact('shift_date', 'count', 'employee_list');

		return response()->json($data)->setCallback( $request->callback );			
	}

	public function getAgentTest(Request $request)
	{
		$employee_number = $request->empid;
		$shift_date = $request->shiftdate;

		$data = compact('employee_number');

		return response()->json($data)->setCallback( $request->callback ); 
	}

	public function getMultipleLILO($row5, $row4, $dst, $pLo)
	{
	
	}

}