<?php

namespace App\Http\Middleware;

use Closure;
use App\Users;

class AllowCors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('token'))
        {            
            $user = Users::where('api_token', $request->token)->get();
            if($user->count()>0)
            {
                $response = $next($request);
                $response->header('Access-Control-Allow-Origin',  '*');
                //$response->header('Access-Control-Allow-Methods:','POST, GET, OPTIONS, PUT, DELETE');
                $response->header('Access-Control-Allow-Methods', 'POST, GET');
                //$response->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization');
                $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));            
                return $response;
            }
            else
            {
                return response()->json(array(
                    'code'      =>  401,
                    'message'   =>  'Unauthorized Action'
                ), 401);
            }
        }
        else
        {
                return response()->json(array(
                    'code'      =>  403,
                    'message'   =>  'Forbidden'
                ), 403);
        }

    }
}
