<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ITSystemsWorkStationInstalledProducts extends Model
{
    protected $connection 	= 'bac_scanner';
    protected $table 		= 'Win_Inventory.Products';
    public $timestamps 		= false;


    public function getInstallDateFormatAttribute()
    {
    	return \Carbon\Carbon::parse( $this->InstallDate )->format('Y-m-d');
    }
}
