<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferLog extends Model
{
    //
    public $table = 'transfer_log';

    public function setUpdatedAtAttribute($value)
	{
	// Do nothing.
	}

	public function scopeTimeReport($query, $start, $end)
	{
		//
		$query->whereBetween('created_at', array($start, $end));

		return $query;
	}

	public function AccountName()
	{
		return $this->hasOne('App\Accounts', 'id', 'acct_id');
	}

	public function SkillName()
	{
		return $this->hasOne('App\Skills', 'id', 'skill_id');
	}

	public function TransactionName()
	{
		return $this->hasOne('App\TransferLogTransactions', 'id', 'transaction_id');
	}

	public function CallDetailsName()
	{
		return $this->hasOne('App\TransferLogCallDetails', 'id', 'calldetails_id');
	}
}
