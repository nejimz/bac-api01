<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class AgentDtrLilo extends Model
{
    protected $connection = 'mysql3';

    protected $table = 'agent_lilo';
    
    public $primaryKey = 'liloID';

    protected $fillable = [ 'liloID','empid', 'avaya', 'shift_date', 
                            'login_date', 'login_time', 'logout_date', 'logout_time', 
                            'loggedon', 'loggedby', 'approved', 'approvedon' ];

    public $timestamps = false;

}