<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UsersEzWatchDoorLogs extends Model
{
     public $table = 'user_ezwatch_door_logs';
     public $timestamps = false;

    protected $fillable = [
        'ntlogin', 'department'
    ];

    public function getEmployeeNameAttribute()
	{
		$ntlogin 	= $this->ntlogin;
		
		return Users::where('ntlogin', $ntlogin)->value('name');
	}

    public function getDepartmentsAttribute()
	{
		$ntlogin = $this->ntlogin;
		
		return UsersEzWatchDoorLogs::where('ntlogin', $ntlogin)->orderBy('department', 'ASC')->get();
	}

    public function getDepartmentNameAttribute()
	{
		$department = $this->department;
		
		return DB::connection('ezpayroll')->table('SectionMaster')->select('SectionName')->where('SectionId', $department)->value('SectionName');
	}
}
