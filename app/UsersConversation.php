<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersConversation extends Model
{
    //
    public $table = 'users_conversation';
    public $timestamps = false;
}
