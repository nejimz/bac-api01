<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ITSystemsWorkstationHardwareInventoryCore extends Model
{
    protected $connection 	= 'bac_scanner';
    protected $table 		= 'AllDevices_Assessment.HardwareInventoryCore';
    public $timestamps 		= false;

    public function getOperatingSystemsCountAttribute()
    {
    	return ITSystemsWorkstationHardwareInventoryCore::where('CurrentOperatingSystem', $this->CurrentOperatingSystem)->count('CurrentOperatingSystem');
    }
}
