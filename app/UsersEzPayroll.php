<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Users;

class UsersEzPayroll extends Model
{
    public $table = 'user_ezpayroll';
    public $timestamps = false;

    protected $fillable = [ 'ntlogin' ];

    public function getEmployeeAttribute()
	{
		$ntlogin 	= $this->ntlogin;
		$employee 	= Users::where('ntlogin', $ntlogin)->first();

     	return $employee;
	}
}
