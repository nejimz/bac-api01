<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReleaseCallValidReasons extends Model
{
    //
    public $table = 'release_call_valid_reasons';
    public $timestamps = false;

    public function log()
    {
    	return $this->hasOne('App\ReleaseCallLog','reason_ID','reason_ID');
    }
}
