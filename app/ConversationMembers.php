<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Users;

class ConversationMembers extends Model
{
    protected $table = "conversation_members";
    public $timestamps = false;

    public function members_conversation()
    {
    	return $this->belongsTo('App\Conversations', 'conversation_id');
    }

    public function members_messages()
    {
    	return $this->belongsToMany('App\ConversationMessages', 'conversation_id');
    }

    public function member_user()
    {
    	return $this->belongsTo('App\Users', 'ntlogin', 'ntlogin');
    }

    public function getCheckNtloginExistsAttribute()
    {
        return Users::whereNtlogin($this->ntlogin)->count();
    }
}
