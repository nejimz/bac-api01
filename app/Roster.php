<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roster extends Model
{
    //
     public $table = 'call_center_roster';
     public $timestamps = false;

     protected $fillable = ['employee_ID', 'organization', 'avaya', 'hire_date', 'supervisor_ID', 'work_pattern', 'tier', 'ac_ID', 'week_ending'];
     
     public function scopeRetrieveProfile($query, $empID)
     {
     	return $query->where('employee_ID', $empID);
     }

     public function scopeMyAgents($query, $empID)
     {
     	return $query->where('supervisor_ID', $empID);
     }

     public function scopeMyCurrentAgents($query, $empID, $week_ending)
     {
          return $query->where('supervisor_ID', $empID)->where('week_ending', $week_ending);
     }

     public function getCurrentTierAttribute()
     {
          return Roster::where('employee_ID', $this->employee_ID)->orderBy('week_ending', 'DESC')->first();
     }

     public function LeaveFiled()
     {
          return $this->hasMany('App\LeaveRequest', 'employee_ID', 'employee_number');
     }
}
