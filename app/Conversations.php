<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversations extends Model
{
    protected $table = "conversations";
    public $timestamps = false;

    public function conversation_members()
    {
    	return $this->hasMany('App\ConversationMembers', 'conversation_id');
    }

    public function conversation_messages()
    {
        //return $this->hasMany('App\ConversationMessages', 'conversation_id');
        return $this->hasMany('App\ConversationMessages', 'conversation_id')->orderBy('created_at', 'DESC');
    }

    public function conversation_messages_first()
    {
        return $this->hasMany('App\ConversationMessages', 'conversation_id')->orderBy('created_at', 'DESC')->take(1);
    }
}
