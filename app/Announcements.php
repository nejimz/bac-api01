<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{
     public $table = 'announcements';
     public $timestamps = true;

    public function user()
    {
    	return $this->belongsTo('App\Users', 'ntlogin', 'ntlogin');
    }
}
