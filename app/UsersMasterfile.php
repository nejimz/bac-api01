<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersMasterfile extends Model
{
    public $table = 'users_masterfile';
    public $timestamps = false;

    protected $fillable = [ 'ntlogin', 'access' ];

    public function getAccessFormatAttribute()
    {
    	$access = $this->access;

    	if ($access == 1)
    	{
    		return 'Admin';
    	}
    	elseif ($access == 2)
    	{
    		return 'Listing and Profile';
    	}
    	elseif ($access == 3)
    	{
    		return 'Listing';
    	}
    }
}
