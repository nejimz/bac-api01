<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersReleaseCallLog extends Model
{
    public $table = 'user_release_call_log';
    public $timestamps = false;

    protected $fillable = [ 'ntlogin' ];

    public function getEmployeeAttribute()
	{
		$ntlogin 	= $this->ntlogin;
		$employee 	= Users::where('ntlogin', $ntlogin)->first();

     	return $employee;
	}
}
