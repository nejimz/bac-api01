<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UsersEzWatch extends Model
{
     public $table = 'user_ezwatch';
     public $timestamps = false;

    protected $fillable = [
        'ntlogin', 'department'
    ];

    public function getEmployeeNameAttribute()
	{
		$ntlogin 	= $this->ntlogin;
		
		return Users::where('ntlogin', $ntlogin)->value('name');
	}

    public function getDepartmentsAttribute()
	{
		$ntlogin = $this->ntlogin;
		
		return UsersEzWatch::where('ntlogin', $ntlogin)->orderBy('department', 'ASC')->get();
	}

    public function getDepartmentNameAttribute()
	{
		$department = $this->department;
		
		return DB::connection('ezpayroll')->table('SectionMaster')->select('SectionName')->where('SectionId', $department)->value('SectionName');
	}
}
