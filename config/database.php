<?php

return [
    #'default' => env('DB_CONNECTION', 'mysql'),
    'default' => env('DB_CONNECTION', 'sqlsrv'),

    'connections' => [

        'mysql' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST', 'localhost'),
            'database'  => env('DB_DATABASE', 'forge'),
            'username'  => env('DB_USERNAME', 'forge'),
            'password'  => env('DB_PASSWORD', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        // BAC-MYSQL01
        'mysql2' => [
            'driver'    => 'mysql',
            'host'      => '172.30.1.202',
            'database'  => 'masterfile',
            'username'  => 'root',
            'password'  => 'p4n4s14t1c',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        // BAC-MYSQL02
        'mysql3' => [
            'driver'    => 'mysql',
            'host'      => '172.30.1.166',
            'database'  => 'dtr',
            'username'  => 'fonck',
            'password'  => '198727',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        // BLUEHOST
        'payslip' => [
            'driver'   => 'mysql',
            'host'     => env('SQLEZW03_HOST', '66.147.244.235'),
            'database' => env('SQLEZW03_DATABASE', 'panasiat_payslip'),
            'username' => env('SQLEZW03_USERNAME', 'panasiat'),
            'password' => env('SQLEZW03_PASSWORD', 'B@col0d2013'),
            'charset'  => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'   => '',
        ],
        // BAC-SCANNER
        'bac_scanner' => [
            'driver'   => 'sqlsrv',
            'host'     => env('DB_HOST', '172.30.1.217\maps'),
            'database' => env('DB_DATABASE', 'maps'),
            'username' => env('DB_USERNAME', 'api01'),
            'password' => env('DB_PASSWORD', 'hC#3mPK?k7nECQ7U'),
            'charset'  => 'utf8',
            'prefix'   => '',
        ],
        // BAC-DB01
        'sqlsrv' => [
            'driver'   => 'sqlsrv',
            'host'     => env('DB_HOST', '172.30.1.205'),
            'database' => env('DB_DATABASE', 'Push'),
            'username' => env('DB_USERNAME', 'bac_api01'),
            'password' => env('DB_PASSWORD', 'vD5:rB_2zc$m3m$4'),
            'charset'  => 'utf8',
            'prefix'   => '',
        ],
        // BAC-EZW01
        'ezpayroll' => [
            'driver'   => 'sqlsrv',
            'host'     => env('SQLEZW01_HOST', '172.30.1.184'),
            'database' => env('SQLEZW01_DATABASE', 'EzPayroll'),
            'username' => env('SQLEZW01_USERNAME', 'dtr'),
            'password' => env('SQLEZW01_PASSWORD', 'dtr'),
            'charset'  => 'utf8',
            'prefix'   => '',
        ],
        // BAC-DEV03
        'ezwatch' => [
            'driver'   => 'sqlsrv',
            'host'     => env('SQLEZW03_HOST', '172.30.1.98'),
            'database' => env('SQLEZW03_DATABASE', 'EzWatcher'),
            'username' => env('SQLEZW03_USERNAME', 'dtr'),
            'password' => env('SQLEZW03_PASSWORD', 'dtr'),
            'charset'  => 'utf8',
            'prefix'   => '',
        ],
        // BAC-EZW05
        'doorlog' => [
            'driver'   => 'sqlsrv',
            'host'     => env('SQLEZW03_HOST', '172.30.1.194'),
            'database' => env('SQLEZW03_DATABASE', 'EzWatcher'),
            'username' => env('SQLEZW03_USERNAME', 'dtr'),
            'password' => env('SQLEZW03_PASSWORD', 'dtr'),
            'charset'  => 'utf8',
            'prefix'   => '',
        ],
        /*'pgsql' => [
            'driver'   => 'pgsql',
            'host'     => env('DB_HOST', 'localhost'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public',
        ],
        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => storage_path('database.sqlite'),
            'prefix'   => '',
        ],*/
    ],
];
