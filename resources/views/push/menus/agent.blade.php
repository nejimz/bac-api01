<br /> <!-- Agent Straight Talk -->
<ul id="pushMainNavLeft" class="side-nav">
  <li><a href="{{ route('transfer_log') }}" target="_blank">Transfer Log</a></li>
  <li><a href="{{ route('release_call_log') }}">Release Call Log</a></li>
  <li><a href="{{ route('agentdtr') }}" target="_blank">My DTR</a></li>
  <li class="">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropLeave" aria-controls="dropLeave" aria-expanded="false" href="{{ route('leave_agent_index') }}">Leave »</a>
    <ul id="dropLeave" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <li>
        <a href="{{ route('leave_agent_create') }}" title="File a Leave"><i class="fa fa-file-o"></i> File a Leave</a>
      </li>
    </ul>
  </li>
</ul>
<script type="text/javascript">$(function(){$(document).foundation();});</script>