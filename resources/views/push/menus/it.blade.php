<br /> 
<ul id="pushMainNavLeft" class="side-nav">
  <?php
  $ezpayroll = App\UsersEzPayroll::where('ntlogin', Auth::user()->ntlogin )->count();
  $ezwatcher = App\UsersEzWatch::where('ntlogin', Auth::user()->ntlogin )->count();
  ?>
  <!-- EzWatch -->
  @if($ezpayroll != 0 || $ezwatcher != 0)
  <li class="{{ \App\Helpers\Menu::activeMenu(['id_information', 'ezwatch', 'ezpayroll_index', 'ezpayroll_create', 'ezpayroll_edit']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropEzWatch" aria-controls="dropEzWatch" aria-expanded="false" href="#">EzWatch »</a>
    <ul id="dropEzWatch" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- ID Info -->
      <li><a href="{{ route('id_information') }}" title="ID Information">ID Information</a></li>
      <!-- EzWatcher -->
      @if($ezwatcher != 0)
      <li><a href="{{ route('ezwatch') }}">EzWatcher</a></li>
      @endif
      <!-- EzPayroll -->
      @if($ezpayroll != 0)
      <li><a href="{{ route('ezpayroll_index') }}">EzPayroll</a></li>
      @endif
    </ul>
  </li>
  @endif
  <!-- Masterfile -->
  @if(App\UsersMasterfile::where('ntlogin', Auth::user()->ntlogin )->count() != 0)
  <li class="{{ \App\Helpers\Menu::activeMenu(['masterfile_index', 'masterfile_create', 'masterfile_upload_agents_create']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropMasterfile" aria-controls="dropMasterfile" aria-expanded="false" href="{{ route('masterfile_index') }}">Masterfile »</a>
    @if(App\UsersMasterfile::where('access', 1)->where('ntlogin', Auth::user()->ntlogin )->count() != 0)
    <ul id="dropMasterfile" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Add Employee -->
      <li><a href="{{ route('masterfile_create') }}" title="Add Employee"><i class="fa fa-plus"></i> Employee</a></li>
      <!-- Upload Agents -->
      <li><a href="{{ route('masterfile_upload_agents_create') }}" title="Upload Agents"><i class="fa fa-upload"></i> Agents</a></li>
    </ul>
    @endif
  </li>
  @endif
  <!-- IT Systems -->
  <li class="{{ \App\Helpers\Menu::activeMenu(['its_ws_index', 'its_ws_summary']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropITSystems" aria-controls="dropITSystems" aria-expanded="false" href="#">IT Systems »</a>
    <ul id="dropITSystems" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Workstations -->
      <li><a href="{{ route('its_ws_index') }}" title="Workstations"><i class="fa fa-desktop"></i> Workstations</a></li>
      <!-- Workstation Summary -->
      <li> <a href="{{ route('its_ws_summary') }}" title="Workstation Summary"><i class="fa fa-list-alt"></i> Workstation Summary</a></li>
      <!-- NT Tracker -->
      <li> <a href="{{ route('nttracker_index') }}" title="NT Tracker"><i class="fa fa-users"></i>NT Tracker</a></li>
    </ul>
  </li>
  <!-- Release Call Log -->
  @if(App\UsersReleaseCallLog::where('ntlogin', Auth::user()->ntlogin )->count() != 0)
  <li class="{{ \App\Helpers\Menu::activeMenu(['rcl_reports']) }}">
    <a href="{{ route('rcl_reports') }}">Release Call Log Reports</a>
  </li>
  @endif
</ul>
<script type="text/javascript">$(function(){$(document).foundation();});</script>