<br /> <!-- Coordinators -->
<ul id="pushMainNavLeft" class="side-nav">
	<!-- Release Call Log -->
	@if(App\UsersReleaseCallLog::where('ntlogin', Auth::user()->ntlogin )->count() != 0)
	<li class="{{ \App\Helpers\Menu::activeMenu(['rcl_reports']) }}">
	    <a href="{{ route('rcl_reports') }}">Release Call Log Reports</a>
	</li>
	@endif
</ul>
<script type="text/javascript">$(function(){$(document).foundation();});</script>