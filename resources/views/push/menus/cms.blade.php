<br /> <!-- CMS Group -->
<ul id="pushMainNavLeft" class="side-nav">
  <li class="{{ \App\Helpers\Menu::activeMenu(['file_uploads', 'upload_agent_schedules']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropFileUploads" aria-controls="dropFileUploads" aria-expanded="false" href="#">File Uploads »</a>
    <ul id="dropFileUploads" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <li><a href="{{ route('upload_roster') }}">Call Center Roster</a></li>
    </ul>
  </li>
  <li class="{{ \App\Helpers\Menu::activeMenu(['rcl_reports']) }}">
      <a href="{{ route('rcl_reports') }}">Release Call Log Reports</a>
  </li>
  <li>
    <a href="{{ route('leave_cms_approval') }}" title="CMS Module">Leave Workforce Module</a>
  </li>
</ul>
<script type="text/javascript">$(function(){$(document).foundation();});</script>