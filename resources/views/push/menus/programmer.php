<br /> <!-- Agent Straight Talk -->
<ul id="pushMainNavLeft" class="side-nav">
  <!-- Control Panel -->
  <li class="{{ \App\Helpers\Menu::activeMenu(['cpanel_index']) }}">
    <a href="{{ route('cpanel_index') }}">Control Panel</a>
  </li>
  <?php
  $ezpayroll = App\UsersEzPayroll::where('ntlogin', $ntlogin )->count();
  $ezwatcher = App\UsersEzWatch::where('ntlogin', $ntlogin )->count();
  $doorlogs = App\UsersEzWatchDoorLogs::where('ntlogin', $ntlogin )->count();
  ?>
  <!-- EzWatch -->
  <?php if($ezpayroll != 0 || $ezwatcher != 0 || $doorlogs !=0) { ?>
  <li class="{{ \App\Helpers\Menu::activeMenu(['id_information', 'ezwatch', 'ezpayroll_index', 'ezpayroll_create', 'ezpayroll_edit']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropEzWatch" aria-controls="dropEzWatch" aria-expanded="false" href="#">EzWatch »</a>
    <ul id="dropEzWatch" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- ID Info -->
      <li><a href="{{ route('id_information') }}" title="ID Information">ID Information</a></li>
      <!-- EzWatcher -->
      <?php if($ezwatcher != 0) { ?>
      <li><a href="{{ route('ezwatch') }}">EzWatcher</a></li>
      <?php } ?>
      <!-- EzPayroll -->
      <?php if($ezpayroll != 0) { ?>
      <li><a href="{{ route('ezpayroll_index') }}">EzPayroll</a></li>
      <?php } ?>
      <!-- Doorlogs -->
      <?php if($doorlogs != 0) { ?>
      <li><a href="{{ route('ezdoorlog') }}">Doorlogs</a></li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
  <!-- Masterfile -->
  <?php if(App\UsersMasterfile::where('ntlogin', $ntlogin )->count() != 0) { ?>
  <li class="{{ \App\Helpers\Menu::activeMenu(['masterfile_index', 'masterfile_create', 'masterfile_upload_agents_create']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropMasterfile" aria-controls="dropMasterfile" aria-expanded="false" href="{{ route('masterfile_index') }}">Masterfile »</a>
    @
    <?php if(App\UsersMasterfile::where('access', 1)->where('ntlogin', $ntlogin )->count() != 0) { ?>
    <ul id="dropMasterfile" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Add Employee -->
      <li><a href="{{ route('masterfile_create') }}" title="Add Employee"><i class="fa fa-plus"></i> Employee</a></li>
      <!-- Upload Agents -->
      <li><a href="{{ route('masterfile_upload_agents_create') }}" title="Upload Agents"><i class="fa fa-upload"></i> Agents</a></li>
    </ul>
    <?php } ?>
  </li>
  <?php } ?>
  <!-- Leave Management System -->
  <li class="{{ \App\Helpers\Menu::activeMenu(['leave_agent_index', 'leave_agent_create', 'leave_cms_approval']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropLeave" aria-controls="dropLeave" aria-expanded="false" href="{{ route('leave_agent_index') }}">Leave »</a>
    <ul id="dropLeave" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- File a Leave -->
      <li><a href="{{ route('leave_agent_create') }}" title="File a Leave"><i class="fa fa-file-o"></i> File a Leave</a></li>
      <!-- CMS Module -->
      <li><a href="{{ route('leave_cms_approval') }}" title="CMS Module"><i class="fa fa-list-alt"></i> Workforce Module</a></li>
    </ul>
  </li>
  <!-- IT Systems -->
  <li class="{{ \App\Helpers\Menu::activeMenu(['its_ws_index', 'its_ws_summary']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropITSystems" aria-controls="dropITSystems" aria-expanded="false" href="#">IT Systems »</a>
    <ul id="dropITSystems" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Workstations -->
      <li><a href="{{ route('its_ws_index') }}" title="Workstations"><i class="fa fa-desktop"></i> Workstations</a></li>
      <!-- Workstation Summary -->
      <li> <a href="{{ route('its_ws_summary') }}" title="Workstation Summary"><i class="fa fa-list-alt"></i> Workstation Summary</a></li>
      <!-- NT Tracker -->
      <li> <a href="{{ route('nttracker_index') }}" title="NT Tracker"><i class="fa fa-users"></i>NT Tracker</a></li>
    </ul>
  </li>
  <!-- Agent DTR -->
  <li>
    <a href="{{ route('agentdtr') }}" target="_blank">Agent DTR</a>
  </li>
  <!-- Release Call Log -->
  <?php if(App\UsersReleaseCallLog::where('ntlogin', $ntlogin )->count() != 0) { ?>
  <li class="{{ \App\Helpers\Menu::activeMenu(['release_call_log', 'reports_CMS', 'reports_supervisor']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropReleaseCall" aria-controls="dropReleaseCall" aria-expanded="false" href="#">Release Call »</a>
    <ul id="dropReleaseCall" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Agent Release Call Log Module -->
      <li><a href="{{ route('release_call_log') }}">Release Call Log</a></li>
      <!-- Release Call Log Module -->
      <li><a href="{{ route('rcl_reports') }}">Reports</a></li>
    </ul>
  </li>
  <?php } ?>
  <!-- Transfer Log -->
  <li class="{{ \App\Helpers\Menu::activeMenu(['transfer_log', 'transfer_log_report']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropTransferLog" aria-controls="dropTransferLog" aria-expanded="false" href="#">Transfer Log »</a>
    <ul id="dropTransferLog" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <li><a href="{{ route('transfer_log') }}">Logger</a></li>
      <li><a href="{{ route('transfer_log_report') }}">Transfer Log Report</a></li>
    </ul>
  </li>
  <!-- Activity Log -->
  <li class="{{ \App\Helpers\Menu::activeMenu(['supervisor_logger']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropActivityLogger" aria-controls="dropActivityLogger" aria-expanded="false" href="#">Activity Log »</a>
    <ul id="dropActivityLogger" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <li><a href="{{ route('supervisor_logger') }}">Supervisor</a></li>
    </ul>
  </li>
  <!-- Case Notation -->
  <li><a href="#">Case Notation</a></li>
  <!-- Schedule -->
  <li><a href="#">Schedule</a></li>
  <!-- Scorecard -->
  <li class="{{ \App\Helpers\Menu::activeMenu(['metrics', 'upload_metrics']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropScorecard" aria-controls="dropScorecard" aria-expanded="false" href="#">Scorecard »</a>
    <ul id="dropScorecard" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Scorecard -->
      <li ><a href="{{ route('metrics') }}">Scorecard</a></li>
      <!-- Upload Scorecard -->
      <li ><a href="{{ route('upload_metrics') }}">Upload Scorecard</a></li>
    </ul>
  </li>
  <!-- DTR -->
  <li><a href="#">DTR</a></li>
  <!-- Seat Plan -->
  <li class="{{ \App\Helpers\Menu::activeMenu(['seatplan_control']) }}">
    <a data-options="is_hover:true; hover_timeout:1000; align:right;" data-dropdown="dropSeatPlan" aria-controls="dropSeatPlan" aria-expanded="false" href="#">Seat Plan »</a>
    <ul id="dropSeatPlan" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Control -->
      <li class=""><a href="{{ route('seatplan_control') }}">Control</a></li>
      <!-- My Business Unit -->
      <li><a href="#">My Business Unit</a></li>
      <!-- My Team -->
      <li><a href="#">My Team</a></li>
    </ul>
  </li>
  <!-- Request -->
  <li>
    <a data-options="is_hover:true; hover_timeout:1000; align:right;" data-dropdown="dropRequest" aria-controls="dropRequest" aria-expanded="false" href="#">Request »</a>
    <ul id="dropRequest" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Leave -->
      <li><a href="#">Leave</a></li>
      <!-- Overtime -->
      <li><a href="#">Overtime</a></li>
    </ul>
  </li>

</ul>
<script type="text/javascript">$(function(){$(document).foundation();});</script>
