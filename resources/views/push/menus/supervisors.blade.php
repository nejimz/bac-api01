<br /> <!-- Supervisors -->
<ul id="pushMainNavLeft" class="side-nav">
	<li class="{{ \App\Helpers\Menu::activeMenu(['team_roster']) }}">
		<a aria-controls="transferCall" aria-expanded="false" href="{{ route('team_roster') }}">My Team</a>
	</li>
	<li class="{{ \App\Helpers\Menu::activeMenu(['transfer_log', 'transfer_log_report']) }}">
		<a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="transferCall" aria-controls="transferCall" aria-expanded="false" href="#">Transfer Log »</a>
	    <ul id="transferCall" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
	    	<li><a href="{{ route('transfer_log_report') }}">Transfer Log Reports</a></li>
	    </ul>
	</li>
	<li class="{{ \App\Helpers\Menu::activeMenu(['rcl_reports']) }}">
	    <a href="{{ route('rcl_reports') }}">Release Call Log Reports</a>
	</li>
</ul>
<script type="text/javascript">$(function(){$(document).foundation();});</script>