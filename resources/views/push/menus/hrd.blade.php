
<br /> 
<ul id="pushMainNavLeft" class="side-nav">
  <?php
  $ezpayroll = App\UsersEzPayroll::where('ntlogin', Auth::user()->ntlogin )->count();
  $ezwatcher = App\UsersEzWatch::where('ntlogin', Auth::user()->ntlogin )->count();
  ?>
  <!-- EzWatch -->
  @if($ezpayroll != 0 || $ezwatcher != 0)
  <li class="{{ \App\Helpers\Menu::activeMenu(['id_information', 'ezwatch', 'ezpayroll_index', 'ezpayroll_create', 'ezpayroll_edit']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropEzWatch" aria-controls="dropEzWatch" aria-expanded="false" href="#">EzWatch »</a>
    <ul id="dropEzWatch" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- EzWatcher -->
      @if($ezwatcher != 0)
      <li><a href="{{ route('ezwatch') }}">EzWatcher</a></li>
      @endif
      <!-- EzPayroll -->
      @if($ezpayroll != 0)
      <li><a href="{{ route('ezpayroll_index') }}">EzPayroll</a></li>
      @endif
    </ul>
  </li>
  @endif
  <!-- Masterfile -->
  @if(App\UsersMasterfile::where('ntlogin', Auth::user()->ntlogin )->count() != 0)
  <li class="{{ \App\Helpers\Menu::activeMenu(['masterfile_index', 'masterfile_create', 'masterfile_upload_agents_create']) }}">
    <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropMasterfile" aria-controls="dropMasterfile" aria-expanded="false" href="{{ route('masterfile_index') }}">Masterfile »</a>
    @if(App\UsersMasterfile::where('access', 1)->where('ntlogin', Auth::user()->ntlogin )->count() != 0)
    <ul id="dropMasterfile" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
      <!-- Add Employee -->
      <li><a href="{{ route('masterfile_create') }}" title="Add Employee"><i class="fa fa-plus"></i> Employee</a></li>
      <!-- Upload Agents -->
      <li><a href="{{ route('masterfile_upload_agents_create') }}" title="Upload Agents"><i class="fa fa-upload"></i> Agents</a></li>
    </ul>
    @endif
  </li>
  @endif
    
</ul>
<script type="text/javascript">$(function(){$(document).foundation();});</script>