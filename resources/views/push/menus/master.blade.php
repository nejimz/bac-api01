<aside class="large-3 columns">
  <ul id="pushMainNavLeft" class="side-nav">
    <li class="{{ \App\Helpers\Menu::activeMenu(['dashboard']) }}"><a href="{{ route('dashboard') }}">Announcement</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['profile']) }}"><a href="{{ route('profile') }}">Profile</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['inbox']) }}"><a href="{{ route('inbox') }}">Inbox&nbsp;<span id="message_notification"></span></a></li>

    <li class="{{ \App\Helpers\Menu::activeMenu(['avaya_one_x_guide']) }}"><a href="{{ route('avaya_one_x_guide') }}">AVAYA one-X Guide</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['paid_leaves']) }}"><a href="{{ route('paid_leaves') }}">Paid Leaves</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['it_security_policy']) }}"><a href="{{ route('it_security_policy') }}">IT Security Policy</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['qa_newsletter']) }}"><a href="{{ route('qa_newsletter') }}">QA Newsletter</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['payslip_index']) }}"><a href="{{ route('payslip_index') }}">Payslip Password Reset</a></li>
  </ul>
  
  <div id="distro_menus">
    <br><p class="text-center"><img src="{{ asset('img/ajax-loader.gif') }}"></p>
  </div>

</aside>